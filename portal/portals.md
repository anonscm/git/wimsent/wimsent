-*- mode: markdown ; coding: utf-8 -*-

General note
============

"??" parts to be checked or completed

The adm/raw access analysis is necessary to have access with a php
script (i.e. with shibboleth authentification).
I'm first checking user access of already existing users.
Only after that I will check user creation via adm/raw
 (e.g. the "too many levels" down has to be fixed both for auth and
 for registration).

What do we want for portals?
============================

authorization for teachers
---------------------------

### in a class

teacher can see everything related to gestion of the students

* **no access** students' activity, students' scores, teaching material

* **access** vote, technical variables, motd

### in a program

teacher can see everything related to teaching material

* **no access** livret (bug), score, activity
The question is that in livret there is also the configuration
of the livret and not only the visualisation of the results.
This part should be in program.
### test zone

everything (but no students are actually registered)

### in a course

students+activity

if a teacher changes the teaching material it is changed in the
program (i.e. in all the courses sharing the same program)

### in a level

create courses/programs, add teachers to course program (but not
create new teachers)


Groupements
===========

Some notes just to build the analogy

Groupement structure is documented in `log/classes/README`

* `class_type=2` groupement (`class_typename=group`)
* `class_type=1` classe dans un groupement ou cours dans un
  établissement ou zone test d'un programme (`class_typename=class`)

in the directory structure

* the groupement is the first level (say `log/classes/123456`)
* the "subclasses" are the second level (say `log/classes/123456/1,
  log/classes/123456/2, ...`)

In a groupement you have 3 kind of users: supervisor, teacher,
student.

access the class via wims auth
------------------------------

* **supervisor**: enter the groupement via
  `module=adm/class/classes&+type=authsupervisor&+class=123456`
* **teacher**: enter the groupement via
  `module=adm/class/classes&+type=authparticipant&+class=123456`
* **student**: enter the groupement via `module=adm/class/classes&+type=authparticipant&+class=123456&+subclass=yes`

**NOTE**: For authentication there is no difference between teacher
and student link, e.g. if a students enters from the teachers login
form, then it is recognised by wims as a student and things work as
you expect, the same holds if a teachers enters from the students
login form. (The option "subclass=yes" is meant for the registration
and it is passed to adm/class/reguser when the students click on the
registration link in the login page.)


### Testing site

[wims2.matapp.unimib.it](https://wims2.matapp.unimib.it) (Italian language! - on the latest svn)

class: 9725195 test group, wims auth

* supervisor login (password "segretissima")

  [https://wims2.matapp.unimib.it/wims/wims.cgi?lang=it&+module=adm/class/classes&+type=authsupervisor&+class=9725195]()

* teacher login ("cazzola", "cazzola")

  [https://wims2.matapp.unimib.it/wims/wims.cgi?lang=it&+module=adm/class/classes&+type=authparticipant&+class=9725195]()

* student login ("pluto", "pluto")

  [https://wims2.matapp.unimib.it/wims/wims.cgi?lang=it&+module=adm/class/classes&+type=authparticipant&+class=9725195&+subclass=yes]()

The links for teachers and students are interchangable (for login of
already registered users).

Access the class via adm/raw
----------------------------

configuration as above (wims auth) + we define a connecting class

    class_connections=+phpidp/available+

note: phpidp contains the lines

    ident_password=abcde
    ident_type=json

### testing site

* supervisor: `job=authuser&qclass=9725195&rclass=available&quser=supervisor`

        lynx -dump "http://127.0.0.1/wims/wims.cgi?module=adm/raw&ident=phpidp&lang=it&passwd=abcde&code=random&job=authuser&qclass=9725195&rclass=available&quser=supervisor"
  this command returns a session id (e.g. B59D26E401) and a url to
  access the class
  ([http://127.0.0.1/wims/wims.cgi?session=B59D26E401.1]() ?? which is wrong, as you shoud have a fully qualified domain name!)

  the class can be accessed (until the session expires) with

  [https://wims2.matapp.unimib.it/wims/wims.cgi?session=B59D26E401.1]()

* teacher: `job=authuser&qclass=9725195&rclass=available&quser=cazzola`

        lynx -dump "http://127.0.0.1/wims/wims.cgi?module=adm/raw&ident=phpidp&lang=it&passwd=abcde&code=random&job=authuser&qclass=9725195&rclass=available&quser=cazzola"
  (and then access to the session as above)

* student: student can access both to the superclass and directly to the class he is registered

        job=authuser&qclass=9725195&rclass=available&quser=pluto

        job=authuser&qclass=9725195_1&rclass=available&quser=pluto

        lynx -dump "http://127.0.0.1/wims/wims.cgi?module=adm/raw&ident=phpidp&lang=it&passwd=abcde&code=random&job=authuser&qclass=9725195&rclass=available&quser=pluto"

        lynx -dump "http://127.0.0.1/wims/wims.cgi?module=adm/raw&ident=phpidp&lang=it&passwd=abcde&code=random&job=authuser&qclass=9725195_1&rclass=available&quser=pluto"

**note**: in the call to adm/raw the `/` in the directory structure becomes `_`

**note**: in order to access via adm/raw to the subclass,
`class_connections=+phpidp/available+` should be set in the subclass
.def file as well

todo (??)
---------

need to check that the var.stat file contained in the session
directory is roughly the same when you access via WIMS or via adm/raw

external authentification with adm/raw
--------------------------------------

When you have WIMS authentication the argument to `quser` is the WIMS
login (above "supervisor", "cazzola" and "pluto").  If you want to use
external login you have to add `option=hashlogin` to the adm/raw call
and the argument to quser is the external login as defined in the user
file).

adm/raw and php
===============

the file index.php in the distribution allows for access to the
superclass (i.e. uses the analougous of the command
`job=authuser&qclass=9725195&rclass=available&quser=pluto` above) with
something like

  `index.php?enter=9725195`

it can be fixed (see fakeauth.php in this directory, the index.php in
phpidp in the svn is updated) so to give direct access to the
subclass, e.g.

  [https://wims2.matapp.unimib.it/fakeauth.php?enter=3362778_1]()

works on the testing site, giving access to the subclass (if the user
is already registered) and registering new users to the subclass
(provided `class_connection` is properly configured in the subclass).

Portals
=======

Structure
---------

Technical documentation of portals is log/classes/README

  %%%%%%%%%%%%%%%%%%%%%%

  `class_type=0` classe individuelle
  `class_type=2` groupement
  `class_type=1` classe dans un groupement ou cours dans un établissement
    ou zone test d'un programme
  `class_type=3` niveau ou classe ou programme dans un établissement
  `class_type=4` etablissement

  `class_typename`: portal, level, program, course, class
  (should add testclass)
  classe dans un etablissement : `$class_typename=class` and `$class_type=3`
  cours dans un etablissement : `$class_typename=course`
  class dans un groupement: `$class_typename=class` and `$class_type=1`
  zone test : /0// isin $wims_class//
  programme ou  classe individuelle ou sous-classe d'un groupement :
     `$class_typename=program` or `$class_type=0` or (`$class_type=1` and `$class_typename!=course` and /0// notin $wims_class//)
  progamme + classe dans un établissement : `$class_type=3` and `class_typename!=level`
  cours ou classe individuelle ou classe groupement (score):
     `$class_typename=course` or `class_type=0` or (`$class_typename=class` and `$class_type=1`)
  %%%%%%%%%%%%%%%%%%%%%%

that is

* "establissement" `class_type=4`, `class_typename=portal`

* "niveau ou classe ou programme dans un établissement" `class_type=3`
  with 3 different "usages" (`class_typename` in level, program, course,
  class)

The description of portals is in the help of the module
`adm/class/gateway`

* La structure d'établissement est bâtie au dessous d'une unique zone
  appelée **portail** (`class_typename=portal`). Elle sert de point
  d'entrée et à la gestion globale de l'établissement. Le vrai travail
  d'enseignement ne doit pas avoir lieu dans cette zone.

* Les zones immédiatement au dessous du portail sont les **niveaux**
  (`class_typename=level`). Un niveau correspond à un ensemble
  d'élèves partageant les mêmes programmes d'enseignement annuels. Les
  niveaux permettent essentiellement la compartimentation de
  l'établissement. Les inscriptions d'élèves ne sont pas acceptées
  dans les niveaux, ni le travail d'enseignement.

* Les **classes** (`class_typename=class`) sont immédiatement au
  dessous des niveaux. Une classe doit correspondre à un groupe
  d'élèves partageant le même emploi du temps. **Les classes sont les
  points d'inscription des élèves**. Chaque élève peut s'inscrire dans
  une ou plusieurs classes.

* À côté des classes, se trouvent les **programmes**
  (`class_typename=program`) qui sont eux aussi immédiatement au
  dessous d'un niveau. Chaque programme rassemble les ressources
  pédagogiques qui peuvent être partagées par une ou plusieurs classes
  du niveau.  Les élèves ne peuvent pas être inscrits dans les
  programmes. **Seuls les enseignants peuvent s'inscrire aux
  programmes** pour préparer les ressources pédagogiques.

* Les **cours** (`class_typename=course`) sont des zones au plus bas
  de l'échelon. Chaque cours appartient à la fois à un programme et à
  une classe. Chaque couple {programme,classe} peut correspondre à un
  cours.  Les ressources pédagogiques d'un programme sont partagées
  par tous ses cours. Et un élève dans une classe peut naviguer parmi
  les cours de la classe. C'est dans les cours que le travail des
  élèves a lieu.  Un cours a un unique ensemble de ressources
  pédagogiques (celui du programme dont il dépend), et un emploi du
  temps unique.

**NOTE**: supervisor access to the portal level (`log/classes/123456`)
teachers can be appointed either to `level`,`program`,`course` or
`class`, students are registered in `class`.

### example and directory structure

[wims2.matapp.unimib.it](https://wims2.matapp.unimib.it)
class 4915065

(supervisor/segretissima
docente/docente
pluto/pluto)

* **portal**: log/classes/4915065 (this can be an access point for
  students, this cannot be a "registration" point for students)

* **level**: 4915065/1  (no student access)

* **class**: 4915065/1/101 (this is the primary access point for students)

* **program**: 4915065/1/201

* **course**: 4915065/1/101/201 (teaching material for students)(101/201
  is the pairing of program=201 and class=101)

**Note**: the access to the zones of the portal is determined by the value
of the variables `user_participate/user_supervise` in the .users files

**examples**:

* students: `user_participate=4915065/1/101`
* teachers: `user_supervise=4915065/1/201,4915065/1,4915065/2/101`

(teachers can have the variable `user_participate` set as well, in this
case they can access such zones as students)

access the class via wims auth
------------------------------

* students access the portal via the link in the "student area"
  `module=adm/class/classes&+type=authparticipant&+class=4915065&+subclass=yes`
  and this gives access to the "class"es the student is registered in

  no differce if students access via the link in the "teacher area"
  (as for groupements, the opion `subclass=yes` is meant only for
  registration), that is
  `module=adm/class/classes&+type=authparticipant&+class=4915065`
  (access to the "class")

  once logged in, by clicking on "home page portal", that is
  `module=adm/class/classes&+type=authparticipant&+auth_user=pluto&+class=4915065`
  the student can access the home page of the portal and is given the
  possibility to register to other "zones"

* teacher access the portal via the link in the "teacher's area"
  i.e. `module=adm/class/classes&+type=authparticipant&+class=4915065`
  and is given access to the "portal" in which he can see the list of
  class, level or program he is registered in.

  (same if teacher access the portal via the link in the "student's
  area")

access via adm/raw
------------------

### modality 1

have everybody access to the portal and let wims do the job to access
to class/level/program and so on

* **supervisor**: `job=authuser&qclass=7314541&rclass=available&quser=supervisor`

      lynx -dump "http://127.0.0.1/wims/wims.cgi?module=adm/raw&ident=phpidp&lang=it&passwd=abcde&code=random&job=authuser&qclass=7314541&rclass=available&quser=supervisor"
  as above the portal can be accessed through the session just created

  [http://wims2.matapp.unimib.it/wims/wims.cgi?session=YZ750C941C.1]()

  **BUT the page shown is not the same as the one shown with standard
  wims auth** e.g. the list of zones in the portal is not shown and to
  access them need to use the link "change zone" in the left menu

  ?? need to check the variables in var.init in the session or the
  auth scripts ??

* **teacher**:
  `job=authuser&qclass=7314541&rclass=available&quser=docente`

  (using `option=hashlogin`)

      lynx -dump "http://127.0.0.1/wims/wims.cgi?module=adm/raw&ident=phpidp&lang=it&passwd=abcde&code=random&job=authuser&qclass=7314541&rclass=available&quser=docente1@docenti.org&option=hashlogin"
  as above the portal can be accessed through the session just created

  [http://wims2.matapp.unimib.it/wims/wims.cgi?session=CT6BBB95EC.1]()
  (list of links to all the program/class/.. the teacher is appointed)

**NOTE** for registering teachers, the php implementation for teacher
  is the option `authtype=teacher`

  [https://wims2.matapp.unimib.it/fakeauth.php?enter=7314541&authtype=teacher]()

* **student**: `job=authuser&qclass=7314541&rclass=available&quser=pluto`

      lynx -dump "http://127.0.0.1/wims/wims.cgi?module=adm/raw&ident=phpidp&lang=it&passwd=abcde&code=random&job=authuser&qclass=7314541&rclass=available&quser=pluto@pluto.org&option=hashlogin"
  as above the portal can be accessed through the session just created

  `http://wims2.matapp.unimib.it/wims/wims.cgi?session=O968209276.1`
  (list of links to all the class/courses the student is registered
  in)

**NOTE** _modality 1_ seems to be working for _existing_ students as
  expected (BUT if students register themselves, they are not
  requested any password for registration: there is **no** student
  registration password at the portal level, so either we change wims
  or need an other modality for registration)

### modality 2

direct access to `class/level/program` and so on (like in the groupement
case the students can access directly to the course)

**NOTE**: need to have `class_connections=+phpidp/available+` in every zone
of the portal you want to access via adm/raw
(?? it would be useful to be able to _send_ such configuration option
to dependent zones)

* teacher with `user_supervise=7314541/1/201,7314541/1,7314541/2/101`
  should be able to access all zones with commands like

    job=authuser&qclass=7314541_1&rclass=available&quser=docente
    job=authuser&qclass=7314541_1_201&rclass=available&quser=docente
    job=authuser&qclass=7314541_1_101&rclass=available&quser=docente

    (note: or the equivalent with `option=hashlogin`)

  **such commands do not works in the current version of adm/raw** **FIXED**

      lynx -dump "http://127.0.0.1/wims/wims.cgi?module=adm/raw&ident=phpidp&lang=it&passwd=abcde&code=random&job=authuser&qclass=7314541_1&rclass=available&quser=docente1@docenti.org&option=hashlogin"
  **error**: "sorry, there shouldn't be a subclass in a simple class" **FIXED**

      lynx -dump "http://127.0.0.1/wims/wims.cgi?module=adm/raw&ident=phpidp&lang=it&passwd=abcde&code=random&job=authuser&qclass=7314541_1_201&rclass=available&quser=docente1@docenti.org&option=hashlogin"
  **error**: "too many subdivisions in class id (3)" **FIXED**

* student `with user_participate=7314541/1/101` should be able to access
  his class with command like

      job=authuser&qclass=7314541_1_101&rclass=available&quser=pluto

  **such command does not work in the current version of adm/raw** **FIXED**

      lynx -dump "http://127.0.0.1/wims/wims.cgi?module=adm/raw&ident=phpidp&lang=it&passwd=abcde&code=random&job=authuser&qclass=7314541_1_101&rclass=available&quser=pluto@pluto.org&option=hashlogin"
  **error**: "too many subdivisions in class id (3)" **FIXED**



Problems with portals
---------------------

* `adm/class/gateway` gives the possibility to backup the portal
  structure in a csv file, but I see no way to restore such
  structure. Once in a level, there seems to be the possibility to use
  csv files to add "classes" but I did not manage to do it (??
  give an example of a working csv file)


Notes specific to the testing site (to be removed)
==================================================

In order to do some testing, on the server

[https://wims2.matapp.unimib.it/wims/wims.cgi?lang=it&+module=adm%2Fclass%2Fclasses&+type=supervisor]()

you will find three different portals.

* **portal wims auth** standard wims authentication and identification
  (password "segreta" almost everywhere) (this is the portal used for
  the tests above: 4915065)

* **portal cas auth** cas authentication and wims idendification
  (configured with bicocca cas, so only supervisor testing possible
  with password "segretissima")(class number: 5774187).

* **portal php auth** php authentification and identification
  (configured with `fakeauth.php` a modified version of the
  `index.php` in this repository (`wimsent/idp`) which skips the
  actual identification part in that file and uses data from a form
  (so that anybody can access as a _fake_ user for testing purposes)
  (class number: 7314541).

  Every time you insert a new (fake) email a new user is created.

  Already registered users

  teachers: docente1@docenti.org, docente2@docenti,org, ...

  students: pluto@pluto.org
