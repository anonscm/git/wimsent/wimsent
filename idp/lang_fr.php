<?php
  // language strings

$enterstring="Entrez";
$entererrormsg="ERREUR&nbsp;: il n'est pas possible de s'enregistrer dans la classe choisie&nbsp;:";
$notexistmsg="La classe demand�e n'existe pas sur le serveur. Choisissez votre classe dans le menu suivant.";
$connectrefusedmsg="L'enregistrement est ferm� pour la classe que vous avez choisie. Suivez les instructions de votre enseignant ou choisissez de nouveau une classe dans le menu.";
$defaulterrormsg="Quand vous contacterez votre enseignant, merci de lui transmettre toutes les informations contenues dans cette page.";
$idploginmsg="<p>Choisissez votre classe dans le menu suivant&nbsp;:</p>";
$classstr="Classe";
$choose="Choisissez&nbsp;!";
$again="Recommencez.";
$enterclass="Entrer dans une classe WIMS";
$wimshomepage="Page d'accueil de WIMS";

// You can customize the page layout here

$iniziopagina="<!doctype html>
<html lang=\"fr\">
<head><title>WIMS - Web Interactive Multipurpose Server</title>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
<link href=\"$css\" rel=\"stylesheet\" type=\"text/css\">
</head><body>
<div id=\"wimsbodybox\">
<div id=\"wimspagebox\">
<div class=\"wimsbody\">";

$textindex="<h1 class=\"wims_title\">$enterclass</h1>";

$finepagina="</div></div></div><div id=\"wimsmenumodubox\" style=\"background-color:$bgcolor\">
<div class=\"wimsmenu\">
<div class=\"menuitem\"><h2>WIMS</h2></div>
<div class=\"menuitem\"><a href=\"$wims?lang=$lang\">$wimshomepage</a></div>
</div>
</div>
<div id=\"wimsmenubox\" style=\"background-color:$bgcolor\">
<div class=\"wimsmenu\">
</div>
</div>\n</body>\n</html>\n";

$classpass="Donner le mot de passe de la classe";
$wrongpass="The password is not correct.";
?>
