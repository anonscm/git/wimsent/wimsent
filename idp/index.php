<?php
// Disable this line if you want to turn on "debug Mode" :
ini_set( "display_errors", 0);

/*

   These php scripts allow for access to a wims server (via the
   adm/raw module) using a central authentication system compatible
   with SimpleSAMLphp or by using the Apache module (mod_auth_openidc).
   These scripts have been tested for use with
   Shibboleth as set up
   - by the University of Milano-Bicocca (e.g. see
     https://www.unimib.it/servizi/service-desk/account-e-servizi-accesso/autenticazione-ateneo).
     Such configuration can be seen in action at
     https://wims.matapp.unimib.it/wims.php
   - by Mathrice as it can been seen in action at https://wims.math.cnrs.fr

   For information and comments: Marina Cazzola (marina.cazzola@unimib.it)

   NEEDS a version of adm/raw later than r12957 (portal&groupements)

   Needed for crypted class passwords (r15473)

 */
/* **************************************************************** */
/*                    SERVER SETUP                                  */
/* **************************************************************** */
//
// change as needed
//
// language: "en", "it", "fr" are currently defined
$lang = "it";

// color: should match definition of ref_bgcolor in wims.conf
$bgcolor = "#ff9999;";
$button_color = "#FFF";

// the fully qualified domain name of the wims server
$wserver = "wims2.matapp.unimib.it"; // no http
//
// the protocol: http or https
$protocol = "https";

/*
  One can choose between

   -- SimpleSamlPhp setup
   You must have a working installation of SimpleSamlPhp and you need to
   check with your idp provider which fields will be returned. The
   following assume that SimpleSamlPhp is installed in
   /usr/local/lib/simplesamlphp and queries a "standard" Shibboleth
   server. $idpfields might need adjustemt according to your setup.

   -- If the variable simplesaml is false, one can protected this PHP script
   by Apache module (mod_auth_openidc)

*/

$ext_auth="simplesaml";
$ext_auth="apache";

// possibility to skip password when registering to a
// portal/groupement (password still needed to register to
// courses/subclasses). On big servers it is better to leave
// $checkportalpass="yes"
$checkportalpass="yes";
// $checkportalpass="no";

if ($ext_auth=="simplesaml"){
  $simplesamlphpconfig = "/usr/local/lib/simplesamlphp/lib/_autoload.php";
  $simplesamlphpauth = "default-sp";
 }
//
// customize: check your idp setup!
//
// email, firstname, lastname are compulsory to create WIMS user,
// other fields might be missing
$idpfields = array(
  'email' => "urn:oid:0.9.2342.19200300.100.1.3",
  'firstname' => "urn:oid:2.5.4.42",
  'lastname' => "urn:oid:2.5.4.4",
  'regnum' => "urn:oid:1.3.6.1.4.18592.1.1.1",
  );
/*
 // facebook example
 $idpfields = array(
 'email' => "facebook.email",
 'firstname' => "facebook.first_name",
 'lastname' => "facebook.last_name",
 'regnum' => "facebook.uid",
 'photourl' => "facebook.pic_square",
 );
*/
/*
 // ////////////////////////////////////////////////////////////
 //
 //  CUSTOMIZABLE VARIABLES (should work with no modifications
 with the phpidp file in the distribution and the php files installed
 on the same server).

 //
 // ////////////////////////////////////////////////////////////

 We assume you have a standard WIMS installation (e.g. wimshome is
 /home/wims) and that the php files are installed on the same
 server. Also we assume that you set up an adm/raw connection by
 copying the phpidp file provided.

 If this is not the case, you might need to adjust some of the
 following variables (see comments further in this file).
*/
//
// WIMS setup:
//
// the following must be set up in
// wimshome/log/classes/.connections/ i.e. the file
// wimshome/log/classes/.connections/$ident must exist
$ident = "phpidp";
// the following must match the "ident_password" in the file
//  wimshome/log/classes/.connections/$ident
$identpwd = "abcde";
// the following must match the "ident_site" in the file
// wimshome/log/classes/.connections/$ident
$ident_site = "127.0.0.1"; // no http
$lwims = "http://$ident_site/wims/wims.cgi";

// file_get_contents more permissive on localhost (when forcing ssl redirection on apache server)

$nossl=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
);

// list of accepted rclasses; may have more than one choice (but putting
// too many element can be costly in terms of wims sessions)
$allowedrclass = array("available");

// web address of this script
$scripturl = "$wserver/index.php";

$entra = "$protocol://" . $scripturl;
$wims = "$protocol://$wserver/wims/wims.cgi";
$css = "$protocol://$wserver/wims/themes/standard/css.css";
$manager = "wims@$wserver";

/* */
/*
 // ////////////////////////////////////////////////////////////
 //
 //         end of customizable part
 //
 // ////////////////////////////////////////////////////////////
 */
//
// functions
//
// check utf-8 (see http://www.phpwact.org/php/i18n/charsets)
function json_decode_nice($json, $assoc = TRUE) {
  return json_decode($json, $assoc);
}
/* checkenter */
/* 
  need to check the parameter given by request/post does not include
  maliciuos code (e.g. Cross-Site Scripting (XSS) vulnerability)
 */
function checkenter($enter) {
  if (!preg_match("!^[0-9/_]*$!",$enter)) {
    die ("invalid code\n");
  }
}
/* make_class_array : */
function make_class_array() {
  $classes = array();
  $count = 0;
  global $lwims, $ident, $identpwd, $allowedrclass, $lang,$nossl;
  foreach ($allowedrclass as $rclass) {
    $output = "";
    $ctlstr = genRandomString();
    $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=listclasses&rclass=$rclass&option=supervisor,description,institution,type";
    $output = file_get_contents($url, false, stream_context_create($nossl));
    $output = utf8_encode($output);
    $output = json_decode($output, TRUE);
    if ($output['status'] . $output['code'] == "OK$ctlstr") {
      foreach ($output['classes_list'] as $index => $values) {
        $classes[$count] = $values;
        $classes[$count]['rclass'] = "$rclass";
        $count++;
      }
    } else {
      if ($output['status'] == "ERROR" and $output['message'] == "there is no class allowed for this server") {
      } else {
        // print $output['status'].$output['code']."\n";
        // print "lynx -dump \"$url\"\n";
        print "<p>Error!</p>";
        print "<pre>\n";
        print_r($output);
        print "</pre>\n";
        exit();
      }
    }
  }
  return $classes;
}
/* getclass: get class (qclass/rclass) properties */
function wims_getclass($qclass, $rclass, ...$options){
  global $lwims, $ident, $identpwd, $lang;
  $ctlstr=genRandomString();
  $opts=implode(",",$options);
  $url="$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=getclass&qclass=$qclass&rclass=$rclass&option=$opts";
  $foutput=file_get_contents($url, false, stream_context_create($nossl));
  $foutput=utf8_encode($foutput);
  $foutput=json_decode($foutput,TRUE);
  if ($foutput['code'] == "$ctlstr") {
    return $foutput;
  }
  else {
    die("Errore");
  }
}
/* geneRandomString : Generate a Random String */
function genRandomString() {
  $length = 10;
  $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
  $string = '';
  for ($p = 0; $p < $length; $p++) {
    $string .= $characters[mt_rand(0, strlen($characters))];
  }
  return $string;
}
/* crypt_pwd: passwords are now crypted */
function crypt_pwd($pwd_in){
  $pwd_output= "*".crypt($pwd_in, 'Nv0');
  return $pwd_output;
}
include("lang_$lang.php");

/* Call simplesamlphp */
if ($ext_auth=="simplesaml") {
  require_once($simplesamlphpconfig);
  // update for SimpleSamlPhp-1.19
  $as = new \SimpleSAML\Auth\Simple($simplesamlphpauth);
}
// set variable $classconn (either through $_REQUEST or via $_POST)
if ((!empty($_REQUEST) && (isset($_REQUEST['enter']))) || (!empty($_POST) && ($_POST['iscrizione'] == "$enterstring"))) {
  if ((isset($_REQUEST['enter']))) {
    $testqclass = $_REQUEST['enter'];
    checkenter($testqclass);
  } elseif ($_POST['iscrizione'] == "$enterstring") {
    $testqclass = $_POST['classconn'];
    checkenter($testqclass);
  } else {
    $testqclass = "NULL";
  }

  // want to use this script to access subclasses (and classes in portals), need to deal with class numbers containing "_"
  $shortarray=explode("_",$testqclass);
  $shortclass=$shortarray[0];

  // check $testqclass exists
  $classes = make_class_array();
  $class_conn_ids = array();
  $class_ids = array();
  foreach ($classes as $index => $values) {
    $class_conn_ids[] = $values['qclass'] . "/" . $values['rclass'];
    $class_ids[$values['qclass']] = $values['rclass'];
  }

  // Case1: $testqclass=qclass/rclass
  if (in_array($testqclass, $class_conn_ids)) {
    $classconn = $testqclass;
  } elseif (array_key_exists($shortclass, $class_ids)) {
    
    // caso 2: $testqclass=qclass (and rclass any of $allowedrclass)
    $classconn = $testqclass . "/" . $class_ids[$shortclass];
  } else {
    print "$iniziopagina\n$textindex\n";
    print "<p>$notexistmsg (" . $testqclass . ") </p>\n";
    print "<p class=\"center\"><a href=\"$protocol://$scripturl\">$again</a></p>\n";
    print "<p class=\"wims_warning\">$defaulterrormsg</p>\n";
    /*
     print "<pre>";
     print_r($class_ids);
     print "<hr>";
     print_r($classes);
     print "</pre>";
    */
    print "$finepagina";
    exit();
  }
}

$isteacher = "no";

$remote_addr=$_SERVER['REMOTE_ADDR'];

if((isset($_POST['authtype'])&&($_POST['authtype'] == "teacher"))||(isset($_REQUEST['authtype'])&& ($_REQUEST['authtype'] == "teacher"))){
  $isteacher = "yes";
 }

if (empty($_POST) && !isset($classconn)) {
  /* initial page: can choose class in a menu */
  print "$iniziopagina\n$textindex\n$idploginmsg\n";
  print "<form action=\"$protocol://$scripturl\" method=\"post\" target=\"_parent\">\n";
  print "<label for=\"classconn\">$classstr:</label><select name=\"classconn\" id=\"classconn\">";

  // fake choice to force student to choose a class in the drop down menu
  print "<option value=\"NULL\">&nbsp;---&nbsp;$choose&nbsp;---&nbsp;</option>";

  $classes = make_class_array();

  foreach ($classes as $index => $values) {
    print "\n<option value=\"" . $values['qclass'] . "/" . $values['rclass'] . "\">" . str_replace("\r\n", "", $values['institution'] . " (" . $values['description'] . ")") . "</option>";
  }
  print "\n</select>\n";
  print "<input class=\"wims_button\"
    style=\"color:$button_color;\"
    type=\"submit\"
    name=\"iscrizione\"
    value=\"$enterstring\"
    />\n";
  print "</form>\n";
  print "$finepagina";
  exit();
 } elseif (isset($classconn)) {

  /*
    $classconn is defined and verified --> require autentification
  */
  $adduser = "";
  if ($ext_auth=="simplesaml") {
    $as->requireAuth();
    $attributes = $as->getAttributes();
    /*
     get data from the idp and authenticate WIMS user (idpfields are
     defined above)
    */

    foreach ($idpfields as $wimsfield => $urnoid) {
      if(isset($attributes[$urnoid]))
      {
        ${$wimsfield}=$attributes[$urnoid][0];
        if ($wimsfield == "firstname" || $wimsfield == "lastname") {
          ${$wimsfield}=ucwords(strtolower(${$wimsfield})," \t\r\n\f\v'");
        }
        $adduser = "$adduser$wimsfield=" . ${$wimsfield} . "\n";
      }
    }
  }
  /*
   * We are getting profile from Apache server ENV values
   */
  else {
    /*
     * using Apache mod_auth_openidc
     */
    if (isset($_SERVER['OIDC_CLAIM_email'])) {
      $email=$_SERVER['OIDC_CLAIM_email'];
      $lastname=$_SERVER['OIDC_CLAIM_family_name'];
      $firstname=$_SERVER['OIDC_CLAIM_given_name'];
    } else {
      /*
      * using Apache Shibboleth module
      */
      $email=$_SERVER['MAIL'];
        if (isset($_SERVER['SN']))
          $lastname=$_SERVER['SN'];
        else $lastname=$_SERVER['CN'];
        if (isset($_SERVER['givenName'])) {
          $firstname=$_SERVER['givenName'];
          if (!isset($_SERVER['SN'])) {
            $lastname = str_replace($firstname,'',$lastname);
          }
        }
        else $firstname=$_SERVER['CN'];
    }
    $adduser="email=".$email."\nfirstname=".$firstname."\nlastname=".$lastname."\n";
  }
} // elseif (isset($classconn))
//
$classconn_arr = explode("/", $classconn);

/*
// check if really needed (with the setup should already have
// $classconn=$qclass/$rclass, might be needed for class_type=3 or 4 ?? )
$perqclass = $classconn_arr[0];
$tmpclass = explode("_", $perqclass);
$qclass = $tmpclass[0];
*/
// we want to deal with "subclasses" so we keep the whole qclass string (including "_")
$qclass = $classconn_arr[0];

//
$rclass = $classconn_arr[1];

//

// reset this variable here
$vrfypasswd = "NO";

// in case of registration to group/portals need to run this file a
// few times
if (isset($_POST['step']) && $_POST['step']=="step1"){
  // need to register student: ask for class password (we are in a
  // group/portal)
  print "$iniziopagina";
  print "<form accept-charset=\"utf-8\" action=\"$protocol://$scripturl\" method=\"post\" target=\"_parent\">\n";
  print "<h1>$classpass</h1>\n";
  print "<input type=\"hidden\" name=\"step\" value=\"step2\">\n";
  print "<input type=\"password\" name=\"classpwd\" />\n";
  print "<input type=\"submit\" name=\"iscrizione\" value=\"$enterstring\" />";
  print "<input type=\"hidden\" name=\"classconn\" value=\"$classconn\" />\n";
  if ($isteacher == "yes") {
    print "<input type=\"hidden\" name=\"authtype\" value=\"teacher\">\n";
  }

  print "</form>\n";
  /*
  print "<pre>\n";
  print "$url\n";
  print_r($_POST);
  print "</pre>\n";
  */
  print "$finepagina";
  exit();
} // step1
if (isset($_POST['step']) && $_POST['step']=="step2"){
  // need to register student: verify given password is correct
  $arr_classconn=explode("/",$_POST['classconn']);
  $output=wims_getclass($arr_classconn[0],$arr_classconn[1],"password");
  if ($output['status'] == "OK") {
    if(crypt_pwd($_POST['classpwd']) == $output['password']){
      $vrfypasswd = "OK";
      $qclass=$arr_classconn[0];
      $rclass=$arr_classconn[1];
    }
    else
    {
      print $iniziopagina;
      print "<p>$wrongpass</p>";
      print $finepagina;
      exit();
    }
  } else {
    print "<p>".$output['status']."</p>";
    print "<p>".$output['message']."</p>";
    exit();
  }
} // step2

// tries to open a WIMS session for quser=$email,qclass=$qclass and
// rclass=$rclass (variables $email,$qclass,$rclass must be defined at
// this point of the script). If user does not exists, we will get an
// error with the hashlogin set, so to be able to create a wims user.
if ($email == "") {
  print "<div class=\"wims_warning\">Invalid setup: variable email is empty</div>\n";
  print "<pre>\n";
  print_r($_SERVER);
  print_r($_SESSION);
  print "</pre>\n";
  exit();
}
$ctlstr = genRandomString();
// $remote_addr might be needed in case of exams
$str_remoteip = urlencode(utf8_decode($remote_addr));
$url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=authuser&qclass=$qclass&rclass=$rclass&quser=$email&option=hashlogin&data1=$str_remoteip";
$output = json_decode_nice(file_get_contents($url, false, stream_context_create($nossl)));
if ($output['status'] . $output['code'] == "OK$ctlstr") {
  // enter WIMS
  $script = "window.parent.location='" . $wims . "?session=" . $output['wims_session'] . "'";
  print("<script type=\"text/javascript\">".$script.";</script>");
  exit();
}
// before registering new user check the class password: test passed
// if (1) class_type= 2 or 4 and $checkportalpass!="yes" (in case
// $checkportalpass="yes" goto to the loop step1/step2 above) (2) the
// correct class password has been given by the student for simple
// class (3) the user is a teacher (authtype=teacher) registering as
// teacher to a superclass or portal (class_type=2 or 4) and the
// correct password has been given
$conntype = "";
foreach ($classes as $index => $values) {
  if ($values['qclass'] . "/" . $values['rclass'] == $classconn) {
    $conntype = $values['type'];
  }
}
if (($conntype == "2" || $conntype == "4") && ($isteacher != "yes")) {
  if($checkportalpass != "yes"){
    $vrfypasswd = "OK";
  }
  else { // student has to give subclass password in order to access
  //
  // TODO: prompt for list of subclasses so that the student can choose
  // (then ask for the corresponding password)
  //
  /* build list of subclasses */
  $ctlstr = genRandomString();
  if ($conntype == "4"){
    $reqoption="classes";
    $fieldname="classes_list";
    $numrecord=array(2,3);
  } else {
    $reqoption="subclasses_list";
    $fieldname=$reqoption;
    $numrecord=array(1,2);
  }
  $output=wims_getclass($qclass,$rclass,$reqoption);

    if($output['status'] == "OK") {
      $subclasses=$output[$fieldname];
      // print menu of available subclasses (?? limit this menu to the
      // ones with the "class_connection" set)
      print "$iniziopagina\n$textindex\n$idploginmsg\n";
      print "<form action=\"$protocol://$scripturl\" method=\"post\" target=\"_parent\">\n";
      print "<label for=\"classconn\">$classstr:</label><select name=\"classconn\" id=\"classconn\">";

      // fake choice to force student to choose a class in the drop down menu
      print "<option value=\"NULL\">&nbsp;---&nbsp;$choose&nbsp;---&nbsp;</option>";
      foreach ($subclasses as $index => $values) {
        // set variable for qclass
        $tmpqclass=str_replace("/","_",$subclasses[$index]['0']);
        if ($conntype == "4"){
          $tmpqclass=$qclass."_".$tmpqclass;
        }
        // set variable for rclass
        $trclass="";
        if(isset($subclasses[$index]['4']) && ($subclasses[$index]['4'] !="")){
          $tmprclass=explode("/",$subclasses[$index]['4']);
          $trclass=str_replace("+","",$tmprclass[1]);
          $trclass="/".$trclass;
        }
        print "\n<option value=\"" . $tmpqclass . $trclass . "\">" . str_replace("\r\n", "", $subclasses[$index][$numrecord[0]] . " (" . $subclasses[$index][$numrecord[1]] . ")") . "</option>";
      }
      print "\n</select>\n";
      print "<input type=\"hidden\" name=\"step\" value=\"step1\">\n";
      print "<input class=\"wims_button\"
    style=\"color:$button_color;\"
    type=\"submit\"
    name=\"iscrizione\"
    value=\"$enterstring\"
    />\n";
      print "</form>\n";
      //      print "<pre>";
      //      print $subclasses[$index]['0'];
      //      print "</pre>";
      //      print "<pre>";
      //      print "subclasses|$fieldname\n";
      //      print_r($subclasses);
      //      print "</pre>";
      print "$finepagina";
      exit();
    } else {
      // print error message
      print "<pre>\n";
      print "Error 1\n";
      // print_r($_POST);
      print_r($output);
      print "</pre>\n";
    }
    exit();
  } // student has to choose subclass
}
else {
  // simple classes (and other cases): if the class password is empty proceed
  $outputpwd=wims_getclass($qclass,$rclass,"type","password");
  if($outputpwd['status']  == "OK"
     &&
     $outputpwd['password']==""
     &&
     ($outputpwd['type']=="0"||$outputpwd['type']=="1"||$outputpwd['type']=="3")
  ) {
    $vrfypasswd="OK";
  } elseif (isset($_POST['classpwd'])
            && 
            // test the posted password is the class password
            $outputpwd['status']  == "OK"
            &&
            $outputpwd['password'] == crypt_pwd($_POST['classpwd'])
  ) {
    $vrfypasswd = "OK";
  }
}

// job=authuser gives error "hashlogin: _wims_login_"
$error = explode(": ", $output['message']);
if (($error[0] == "hashlogin") && ($vrfypasswd == "OK")) {

  //
  // ERROR: no such user --> adduser
  //
  $logwims = $error[1];
  $pwd = genRandomString();
  $pwd = "*" . crypt($pwd, 'Nv0');
  $adduser = $adduser . "password=$pwd\nexternal_auth=$email";
  if ($isteacher == "yes") {
        $adduser = $adduser . "\nsupervisable=yes";
  }

  //
  $adduser = urlencode(utf8_decode($adduser));
  $ctlstr = genRandomString();
  $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=adduser&qclass=$qclass&rclass=$rclass&quser=$logwims&data1=$adduser";
  $output = json_decode_nice(file_get_contents($url, false, stream_context_create($nossl)));
  if ($output['status'] . $output['code'] == "OK$ctlstr") {

    // open an authenticated session
    $ctlstr = genRandomString();
    $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=authuser&qclass=$qclass&rclass=$rclass&quser=$logwims";
    $output = json_decode_nice(file_get_contents($url, false, stream_context_create($nossl)));
    if ($output['status'] . $output['code'] == "OK$ctlstr") {

      // enter WIMS
      $script = "window.parent.location='" . $wims . "?session=" . $output['wims_session'] . "';";
      print("<script type=\"text/javascript\">".$script."</script>");
      exit();
    } else {

      // could not open the session
      print "$iniziopagina\n$textindex\n";
      print "$entererrormsg\n<pre>\n";
      print $output['status'] . ": " . $output['message'];
      print "\n</pre>\n";
      switch (substr($output['message'], 0, 30)) {
      case "modification of class not allowed":
      case "modification of class not allo":
      case "connection refused by requeste":
      print "<p>$connectrefusedmsg</p>\n";
      break;

      default:
      echo "<p>$defaulterrormsg</p>\n";
    }
    print "$finepagina\n";
    exit();
  }
} else {
    // could not create user
    print "$iniziopagina\n$textindex\n";
    print "$entererrormsg\n<pre>\n";
    print $output['status'] . ": " . $output['message'];
    print "\n</pre>\n";
    switch (substr($output['message'], 0, 30)) {
    case "modification of class not allowed":
    case "modification of class not allo":
    case "connection refused by requeste":
    print "<p>$connectrefusedmsg</p>\n";
    break;

    default:
      echo "<p>$defaulterrormsg</p>\n";
  }
  /*
  print "<pre>";
  print_r($_POST);
  print "</pre>";
  */
  print "$finepagina\n";
  exit();
  }
}

// ask for class password
elseif ($error[0] == "hashlogin") {
  print "$iniziopagina";
  print "<form accept-charset=\"utf-8\" action=\"$protocol://$scripturl\" method=\"post\" target=\"_parent\">\n";
  print "<h1>$classpass</h1>\n";
  print "<input type=\"password\" name=\"classpwd\" />\n";
  print "<input type=\"submit\" name=\"iscrizione\" value=\"$enterstring\" />";
  print "<input type=\"hidden\" name=\"classconn\" value=\"$classconn\" />\n";
  if ($isteacher == "yes") {
    print "<input type=\"hidden\" name=\"authtype\" value=\"teacher\">\n";
  }

  print "</form>\n";
  /*
  print "<pre>\n";
  print_r($output);
    print "conntype=$conntype\n";
    print "classconn=$classconn\n";
  print_r($classes);
  print "</pre>\n";
  */
  print "$finepagina";
} else {
  // need better handling of errors
  print "<div>The script is not properly configured</div>";
  print "User email: \n|$email|\n";
  print "User classconn: \n|$classconn|\n";
  print "<pre>\n";
  print_r($output);
  //         print "$conntype\n";
  //         print "$classconn\n";
  //         print_r($classes);
  print "</pre>\n";
}

?>
