<?php 
  // language strings 

$enterstring="Enter";
$entererrormsg="ERROR: it is not possible to register to the chosen class:";
$notexistmsg="The requested class does not exist on the server. Choose the class in the drop down menu.";
$connectrefusedmsg="Registration is closed for the chosen class. Check your teacher instructions or choose again your class in the drop down menu.";
$defaulterrormsg="When you contact your teacher be sure to transmit all the infmartion contained in this page.";
$idploginmsg="<p>Choose your class in the drop down menu:</p>";
$classstr="Class";
$choose="choose!";
$again="Again";
$enterclass="Enter a WIMS class";
$wimshomepage="Home page WIMS";

// You can customize the page layout here

$iniziopagina="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">	    
<html><head><title>WWW Interactive Multipurpose Server</title>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
<link href=\"$css\" rel=\"stylesheet\" type=\"text/css\"></head><body>
<div id=\"wimstopbox\" style=\"background-color:$bgcolor\">
<div class=\"wimsmenu\">
</div></div>
<div id=\"wimsbodybox\">
<div id=\"wimspagebox\">
<div class=\"wimsbody\">";

$textindex="<h1 class=\"wims_title\">$enterclass</h1>
<p><br></p>
";

$finepagina="</div></div></div><div id=\"wimsmenumodubox\" style=\"background-color:$bgcolor\">
<div class=\"wimsmenu\">
<div class=\"menuitem\"><h2>WIMS</h2></div>
<div class=\"menuitem\"><a href=\"$wims?lang=$lang\">$wimshomepage</a></div>
</div>
</div>
<div id=\"wimsmenubox\" style=\"background-color:$bgcolor\">
<div class=\"wimsmenu\">
</div>
</div>\n</body>\n</html>\n";

$classpass="Please insert the class password";
$wrongpass="The password is not correct.";
?>
