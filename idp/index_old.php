<?php
// Disable this line if you want to turn on "debug Mode" :
ini_set( "display_errors", 0);

/*

   These php scripts allow for access to a wims server (via the
   adm/raw module) using a central authentication system compatible
   with SimpleSAMLphp or by using the Apache module (mod_auth_openidc).
   These scripts have been tested for use with
   Shibboleth as set up
   - by the University of Milano-Bicocca (e.g. see
     http://web1.si.unimib.it/autenticazione/).
     Such configuration can be seen in action at 
     https://wims.matapp.unimib.it/wims.php
   - by Mathrice as it can been seen in action at https://wims.math.cnrs.fr

   For information and comments: Marina Cazzola (marina.cazzola@unimib.it)

NEEDS a version of adm/raw later than r5156

 */
/* **************************************************************** */
/*                    SERVER SETUP                                  */
/* **************************************************************** */
//
// change as needed
//
// language: "en", "it", "fr" are currently defined
$lang = "it";

// color: should match definition of ref_bgcolor in wims.conf
$bgcolor = "#ff9999;";
$button_color = "#FFF";

// the fully qualified domain name of the wims server
$wserver = "wims2.matapp.unimib.it";
// no http

// the protocol: http or https
$protocol = "https";

/*
  One can choose between

   -- SimpleSamlPhp setup
   You must have a working installation of SimpleSamlPhp and you need to
   check with your idp provider which fields will be returned. The
   following assume that SimpleSamlPhp is installed in
   /usr/local/lib/simplesamlphp and queries a "standard" Shibboleth
   server. $idpfields might need adjustemt according to your setup.

   -- If the variable simplesaml is false, one can protected this PHP script
   by Apache module (mod_auth_openidc)
 */

$simplesaml=true;

if ($simplesaml){
  $simplesamlphpconfig = "/usr/local/lib/simplesamlphp/lib/_autoload.php";
  $simplesamlphpauth = "default-sp";
}
//
// customize: check your idp setup!
//
// email, firstname, lastname are compulsory to create WIMS user,
// other fields might be missing
$idpfields = array(
  'email' => "urn:oid:0.9.2342.19200300.100.1.3",
  'firstname' => "urn:oid:2.5.4.42",
  'lastname' => "urn:oid:2.5.4.4",
  'regnum' => "urn:oid:1.3.6.1.4.18592.1.1.1",
);
/*
// facebook example
$idpfields = array(
  'email' => "facebook.email",
  'firstname' => "facebook.first_name",
  'lastname' => "facebook.last_name",
  'regnum' => "facebook.uid",
  'photourl' => "facebook.pic_square",
);
 */
/*
// ////////////////////////////////////////////////////////////
//
//  CUSTOMIZABLE VARIABLES (should work with no modifications
with the phpidp file in the distribution and the php files installed
on the same server).

//
// ////////////////////////////////////////////////////////////

We assume you have a standard WIMS installation (e.g. wimshome is
/home/wims) and that the php files are installed on the same
server. Also we assume that you set up an adm/raw connection by
copying the phpidp file provided.

If this is not the case, you might need to adjust some of the
following variables (see comments further in this file).
 */
//
// WIMS setup:
//
// the following must be set up in
// wimshome/log/classes/.connections/ i.e. the file
// wimshome/log/classes/.connections/$ident must exist
$ident = "phpidp";
// the following must match the "ident_password" in the file
//  wimshome/log/classes/.connections/$ident
$identpwd = "abcde";
// the following must match the "ident_site" in the file
// wimshome/log/classes/.connections/$ident
$ident_site = "127.0.0.1"; // no http
$lwims = "http://$ident_site/wims/wims.cgi";

// list of accepted "rclass"es; may have more than one choice (but putting
// too many element can be costy in terms of wims sessions)
$allowedrclass = array("available");

// web address of this script
$scripturl = "$wserver/index.php";

$entra = "$protocol://" . $scripturl;
$wims = "$protocol://$wserver/wims/wims.cgi";
$css = "http://$wserver/wims/themes/standard/css.css";
$manager = "wims@$wserver";

/* */
/*
// ////////////////////////////////////////////////////////////
//
//         end of customizable part
//
// ////////////////////////////////////////////////////////////
*/
//
// functions
//
// check utf-8 (see http://www.phpwact.org/php/i18n/charsets)
function json_decode_nice($json, $assoc = TRUE) {
  return json_decode($json, $assoc);
}
/* make_class_array : */
function make_class_array() {
  $classes = array();
  $count = 0;
  global $lwims, $ident, $identpwd, $allowedrclass, $lang;
  foreach ($allowedrclass as $rclass) {
    $output = "";
    $ctlstr = genRandomString();
    $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=listclasses&rclass=$rclass&option=supervisor,description,institution,type";
    $output = file_get_contents($url);
    $output = utf8_encode($output);
    $output = json_decode($output, TRUE);
    if ($output['status'] . $output['code'] == "OK$ctlstr") {
      foreach ($output['classes_list'] as $index => $values) {
        $classes[$count] = $values;
        $classes[$count]['rclass'] = "$rclass";
        $count++;
      }
    } else {
      if ($output['status'] == "ERROR" and $output['message'] == "there is no class allowed for this server") {
      } else {
        print $output['status'].$output['code']."\n";
	//       print "lynx -dump \"$url\"\n";
	print "<p>Error!</p>";
        print "<pre>\n";
        print_r($output);
        print "</pre>\n";
        exit();
      }
    }
  }
  return $classes;
}

/* geneRandomString : Generate a Random String */
function genRandomString() {
  $length = 10;
  $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
  $string = '';
  for ($p = 0; $p < $length; $p++) {
    $string .= $characters[mt_rand(0, strlen($characters))];
  }
  return $string;
}
include("lang_$lang.php");

/* Call simplesamlphp */
if ($simplesaml) {
  require_once($simplesamlphpconfig);
  $as = new SimpleSAML_Auth_Simple($simplesamlphpauth);
}
// set variable $classconn (either through $_REQUEST or via $_POST)
  if ((!empty($_REQUEST) && (isset($_REQUEST['enter']))) || (!empty($_POST) && ($_POST['iscrizione'] == "$enterstring"))) {
    if ((isset($_REQUEST['enter']))) {
      $testqclass = $_REQUEST['enter'];
    } elseif ($_POST['iscrizione'] == "$enterstring") {
      $testqclass = $_POST['classconn'];
    } else {
      $testqclass = "NULL";
    }

// want to use this script to access subclasses (and classes in portals), need to deal with class numbers containing "_"
   $shortarray=explode("_",$testqclass);
   $shortclass=$shortarray[0];

  // check $testqclass exists
  $classes = make_class_array();
  $class_conn_ids = array();
  $class_ids = array();
  foreach ($classes as $index => $values) {
    $class_conn_ids[] = $values['qclass'] . "/" . $values['rclass'];
    $class_ids[$values['qclass']] = $values['rclass'];
  }

  // Case1: $testqclass=qclass/rclass
  if (in_array($testqclass, $class_conn_ids)) {
    $classconn = $testqclass;
  } elseif (array_key_exists($shortclass, $class_ids)) {

    // caso 2: $testqclass=qclass (and rclass any of $allowedrclass)
    $classconn = $testqclass . "/" . $class_ids[$shortclass];
  } else {
    print "$iniziopagina\n$textindex\n";
    print "<p>$notexistmsg (" . $testqclass . ") </p>\n";
    print "<p class=\"center\"><a href=\"$protocol://$scripturl\">$again</a></p>\n";
    print "<p class=\"wims_warning\">$defaulterrormsg</p>\n";
    /*
       print "<pre>";
       print_r($class_ids);
       print "<hr>";
       print_r($classes);
       print "</pre>";
     */
    print "$finepagina";
    exit();
  }
}

$isteacher = "no";
if((isset($_POST['authtype'])&&($_POST['authtype'] == "teacher"))||(isset($_REQUEST['authtype'])&& ($_REQUEST['authtype'] == "teacher"))){
  $isteacher = "yes";
}

if (empty($_POST) && !isset($classconn)) {
  /* pagina iniziale, scelta del corso in un menu a tendina */
  print "$iniziopagina\n$textindex\n$idploginmsg\n";
  print "<form action=\"$protocol://$scripturl\" method=\"post\" target=\"_parent\">\n";
  print "<label for=\"classconn\">$classstr:</label><select name=\"classconn\" id=\"classconn\">";

  // fake choice to force student to choose a class in the drop down menu
  print "<option value=\"NULL\">&nbsp;---&nbsp;$choose&nbsp;---&nbsp;</option>";

  $classes = make_class_array();

  foreach ($classes as $index => $values) {
    print "\n<option value=\"" . $values['qclass'] . "/" . $values['rclass'] . "\">" . str_replace("\r\n", "", $values['institution'] . " (" . $values['description'] . ")") . "</option>";
  }
  print "\n</select>\n";
  print "<input class=\"wims_button\"
    style=\"color:$button_color;\"
    type=\"submit\"
    name=\"iscrizione\"
    value=\"$enterstring\"
    >\n";
  print "</form>\n";
  print "$finepagina";
  exit();
} elseif (isset($classconn)) {

  /*
     $classconn is defined and verified --> require autentification
   */
  $adduser = "";
  if ($simplesaml) {
    $as->requireAuth();
    $attributes = $as->getAttributes();
  /*
     get data from the idp and authenticate WIMS user (idpfields are
     defined above)
   */

    foreach ($idpfields as $wimsfield => $urnoid) {
      ${$wimsfield}=$attributes[$urnoid][0];
      if ($wimsfield == "firstname" || $wimsfield == "lastname") {
      ${$wimsfield}=ucwords(strtolower(${$wimsfield}));
      }
      $adduser = "$adduser$wimsfield=" . $ {$wimsfield} . "\n";
    }
  }
  /*
   * We are getting profile from Apache server ENV values
   */
  else {
    /*
     * using Apache mod_auth_openidc
     */
    if (isset($_SERVER['OIDC_CLAIM_email'])) {
      $email=$_SERVER['OIDC_CLAIM_email'];
      $lastname=$_SERVER['OIDC_CLAIM_family_name'];
      $firstname=$_SERVER['OIDC_CLAIM_given_name'];
    } else {
    /*
     * using Apache Shibboleth module
     */
      $email=$_SERVER['MAIL'];
      if (isset($_SERVER['SN']))
        $lastname=$_SERVER['SN'];
      else $lastname=$_SERVER['CN'];
      if (isset($_SERVER['givenName'])) {
        $firstname=$_SERVER['givenName'];
        if (!isset($_SERVER['SN'])) {
          $lastname = str_replace($firstname,'',$lastname);
        }
      }
      else $firstname=$_SERVER['CN'];
    }
    $adduser="email=".$email."\nfirstname=".$firstname."\nlastname=".$lastname."\n";
  }
}
  //
  $classconn_arr = explode("/", $classconn);

/*
  // check if really needed (with the setup should already have
  // $classconn=$qclass/$rclass, might be needed for class_type=3 or 4 ?? )
  $perqclass = $classconn_arr[0];
  $tmpclass = explode("_", $perqclass);
  $qclass = $tmpclass[0];
*/
// we want to deal with "subclasses" so we keep the whole qclass string (including "_")
$qclass = $classconn_arr[0];

  //
  $rclass = $classconn_arr[1];

  //

  //
  $ctlstr = genRandomString();

  // tries to open a WIMS session (if user does not exists, we will get
  // an error)
  //$url="$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=authuser&qclass=$qclass&rclass=$rclass&quser=$logwims";
  if ($email == "") {
    print "<div class=\"wims_warning\">Invalid setup: variable email is empty</div>\n";
    print "<pre>\n";
    print_r($_SERVER);
    print_r($_SESSION);
    print "</pre>\n";
    exit();
  }
  $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=authuser&qclass=$qclass&rclass=$rclass&quser=$email&option=hashlogin";
  $output = json_decode_nice(file_get_contents($url));
  if ($output['status'] . $output['code'] == "OK$ctlstr") {

    // enter WIMS
    $script = "window.parent.location='" . $wims . "?session=" . $output['wims_session'] . "'";
    print("<script type=\"text/javascript\">".$script.";</script>");
    exit();
  } else {
    // before registering new user check the class password: test
    // passed if (1) class_type= 2 or 4 (that is no password check
    // to register to a superclass or portal, registration to the
    // subclasses is dealt by wims) (2) the correct class password
    // has been given by the student (3) the user is a teacher
    // (authtype=teacher) registering as teacher to a superclass
    // or portal (class_type=2 or 4) and the correct password has
    // been given
    $vrfypasswd = "NO";
    $conntype = "";
    foreach ($classes as $index => $values) {
      if ($values['qclass'] . "/" . $values['rclass'] == $classconn) {
        $conntype = $values['type'];
      }
    }
    if (($conntype == "2" || $conntype == "4") && ($isteacher != "yes")) {
      $vrfypasswd = "OK";
    } elseif (isset($_POST['classpwd'])) {

      // test the posted password is the class password
      $ctlstr = genRandomString();
      $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=getclass&qclass=$qclass&rclass=$rclass&option=password";
      $outputtmp = json_decode_nice(file_get_contents($url));
      if ($outputtmp['status'] . $outputtmp['code'] == "OK$ctlstr" && $outputtmp['password'] == $_POST['classpwd']) {
        $vrfypasswd = "OK";
      }
    }

    // job=authuser gives error "hashlogin: _wims_login_"
    $error = explode(": ", $output['message']);
    if (($error[0] == "hashlogin") && ($vrfypasswd == "OK")) {

      //
      // ERROR: no such user --> adduser
      //
      $logwims = $error[1];
      $pwd = genRandomString();
      $pwd = "*" . crypt($pwd, 'Nv0');
      $adduser = $adduser . "password=$pwd\nexternal_auth=$email";
      if ($isteacher == "yes") {
        $adduser = $adduser . "\nsupervisable=yes";
      }

      //
      $adduser = urlencode(utf8_decode($adduser));
      $ctlstr = genRandomString();
      $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=adduser&qclass=$qclass&rclass=$rclass&quser=$logwims&data1=$adduser";
      $output = json_decode_nice(file_get_contents($url));
      if ($output['status'] . $output['code'] == "OK$ctlstr") {

        // open an authenticated session
        $ctlstr = genRandomString();
        $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=authuser&qclass=$qclass&rclass=$rclass&quser=$logwims";
        $output = json_decode_nice(file_get_contents($url));
        if ($output['status'] . $output['code'] == "OK$ctlstr") {

          // enter WIMS
          $script = "window.parent.location='" . $wims . "?session=" . $output['wims_session'] . "';";
          print("<script type=\"text/javascript\">".$script."</script>");
          exit();
        } else {

          // could not open the session
          print "$iniziopagina\n$textindex\n";
          print "$entererrormsg\n<pre>\n";
          print $output['status'] . ": " . $output['message'];
          print "\n</pre>\n";
          switch (substr($output['message'], 0, 30)) {
            case "modification of class not allowed":
            case "modification of class not allo":
            case "connection refused by requeste":
              print "<p>$connectrefusedmsg</p>\n";
              break;

            default:
              echo "<p>$defaulterrormsg</p>\n";
          }
          print "$finepagina\n";
          exit();
        }
      } else {

        // could not create user
        print "$iniziopagina\n$textindex\n";
        print "$entererrormsg\n<pre>\n";
        print $output['status'] . ": " . $output['message'];
        print "\n</pre>\n";
        switch (substr($output['message'], 0, 30)) {
          case "modification of class not allowed":
          case "modification of class not allo":
          case "connection refused by requeste":
            print "<p>$connectrefusedmsg</p>\n";
            break;

          default:
            echo "<p>$defaulterrormsg</p>\n";
        }
        print "$finepagina\n";
        exit();
      }
    }

    // ask for class password
    elseif ($error[0] == "hashlogin") {
      print "$iniziopagina";
      print "<form accept-charset=\"utf-8\" action=\"$protocol://$scripturl\" method=\"post\" target=\"_parent\">\n";
      print "<h1>$classpass</h1>\n";
      print "<input type=\"password\" name=\"classpwd\" />\n";
      print "<input type=\"submit\" name=\"iscrizione\" value=\"$enterstring\" \>";
      print "<input type=\"hidden\" name=\"classconn\" value=\"$classconn\" \>\n";
      if ($isteacher == "yes") {
        print "<input type=\"hidden\" name=\"authtype\" value=\"teacher\">\n";
      }
      print "</form>\n";

      /*
         print "<pre>\n";
         print_r($output);
         print "$conntype\n";
         print "$classconn\n";
         print_r($classes);
         print "</pre>\n";
       */
      print "$finepagina";
    } else {
      // need better handling of errors
      print "<div>The script is not properly configured</div>";
      print "User email: \n|$email|\n";
         print "<pre>\n";
         print_r($output);
         print "</pre>\n";
    }
    exit();
  }
?>
