<?php
  // language strings

$enterstring="entra";
$entererrormsg="Si è verificato un errore: non si è iscritti al corso scelto e non è stato possibile effettuare l'iscrizione:";
$notexistmsg="Il corso richiesto non esiste. Siete sicuri di aver selezionato il corso corretto nel menu a tendina?";
$connectrefusedmsg="Il corso al momento non accetta iscrizioni. &Egrave; necessario
attendere istruzioni in merito da parte del proprio docente. Siete sicuri di aver selezionato il corso corretto nel menu a tendina?";
$defaulterrormsg="Se in caso d'errore si contatta il proprio docente, si raccomanda di trasmettere tutte le informazioni contenute in questa pagina.";
// no newlines in the following variable
$idploginmsg="<p>Scegli il corso nel menu a tendina:</p>";
$classstr="Corso";
$choose="scegliere!";
$again="Riprova";
$enterclass="Ingresso in un corso WIMS";
$wimshomepage="Home page WIMS";

// You can customize the page layout here

$iniziopagina="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">
<html lang=\"it\"><head><title>WWW Interactive Multipurpose Server</title>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
<link href=\"$css\" rel=\"stylesheet\" type=\"text/css\"></head><body>
<div id=\"wimstopbox\" style=\"background-color:$bgcolor\">
<div class=\"wimsmenu\">
</div></div>
<div id=\"wimsbodybox\">
<div id=\"wimspagebox\">
<div class=\"wimsbody\">";

$textindex="<h1 class=\"wims_title\">$enterclass</h1>
<p><br></p>
";

$finepagina="</div></div></div><div id=\"wimsmenumodubox\" style=\"background-color:$bgcolor\">
<div class=\"wimsmenu\">
<div class=\"menuitem\"><h2>WIMS</h2></div>
<div class=\"menuitem\"><a href=\"$wims?lang=$lang\">$wimshomepage</a></div>
</div>
</div>
<div id=\"wimsmenubox\" style=\"background-color:$bgcolor\">
<div class=\"wimsmenu\">
</div>
</div>\n</body>\n</html>\n";

$classpass="Inserire la password del corso";
$wrongpass="La password inserita non è corretta.";
?>
