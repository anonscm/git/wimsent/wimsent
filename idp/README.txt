Quick list

These instructions assume you have a standard wims server
(e.g. wimshome is /home/wims) and that the php files are installed on
the same server.

1a) install simplesamlphp (http://simplesamlphp.org/) and configure it
   (you need to have a working installation of SimpleSamlPhp, consult
   your idp provider). We assume you installed simplesamlphp in
   /usr/local/lib/simplesamlphp

1b) or follow the instruction in
  https://plm.wiki.math.cnrs.fr/servicesnumeriques/identites/clients/wims

2) configure wims class connections: copy the file phpidp in
   $wimshome/log/classes/.connections (for a "production" site it is
   advisable to change ident_password, in this case you need to
   configure $identpwd variable in index.php)

3) copy the index.php and lang_*.php files on the server (we assume
   you copy the file in the root directory of the http server, namely
   /var/www, if this is not the case you need to change $scripturl
   variable in index.php)

4) check configuration in index.php, see comments in file. Default
   settings should work for a standard installation. You will need to
   set $wserver that has to match the fully qualified domain name of
   your wims server (typically wims.somedomain.something).

5) in order to use this kind of external authentication classes
   should have these variables set:

   !set class_authidp=php;php
   !set class_php_auth=_web_address_of_script_/index.php
   !set class_connections=+phpidp/available+

   These variables can be set in the "Authentication" page in the
   "Class Config" module, see the "Configure" and "PHP Settings"
   sections in that page (site default can be set by the
   webmaster). For a fine tuning of the third variable you can also
   check the "Other servers" page in the "Class Config" module
   (details in the documentation of the adm/raw module).

   If you wish to use remote classes other than the preset value
   "available" you need to add them to the variable $allowedrclass in
   the file index.php.

6) If you access the script with no option

   http://_hostname_of_wims_server_/index.php --> access to wims classes via idp

   you get a drop down menu of classes available and you can access
   them directly. You can use the script in this way to test the
   configuration of the adm/raw module.

