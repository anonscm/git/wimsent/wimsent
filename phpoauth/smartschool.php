<?php
/**
 *   OAuth exchange for Smartschool
 *    (see https://www.smartschool.be/fr/oauth/)
 */


/*********************
 * Smartschool setup *
 *********************/

/**
 * the following need to be changed according
 *  https://www.smartschool.be/fr/oauth/#toggle-id-8
 */
$smsch="XXXXX.smartschool.be";

/**
 * To obtain client_id and client_secret you need to fill in the form
 * at https://www.smartschool.be/fr/oauth/
 */
$client_id="AAAAAAAAAAAAAAA";
$client_secret="BBBBBBBBBBBB";


/*********************
 * WIMS setup
 *********************/

$wimsserver="openwims.matapp.unimib.it";

/**
 * the following must be set up in
 * wimshome/log/classes/.connections/ i.e. the file
 * wimshome/log/classes/.connections/$ident must exist
 */
$ident="phpidp";
$identpwd="abcde";
$rclass="smart";
$bgcolor = "#00753b";
$button_color = "#000000";
$lang="fr";

/**
 * End of customizable part (no further modification are needed)
 */


$auth_uri="https://$smsch/OAuth";
$token_uri="https://$smsch/OAuth/index/token";
$userinfo_uri="https://$smsch/Api/V1/userinfo";

$scripturl="https://$wimsserver/smartschool.php";
$css="https://$wimsserver/wims/themes/standard/css.css";
$gdpr="https://$wimsserver/wims/wims.cgi?lang=fr&+module=adm%2Flight&+phtml=cgu.phtml";

$wims="https://$wimsserver/wims/wims.cgi";
$lwims="http://127.0.0.1/wims/wims.cgi";

/**
 * This variables needs to be stored in a session to test integrity of
 * the exchange with the oauth server
 */
session_name('GWIMS');
session_start();
if (!isset($_SESSION['secure'])) {
    $_SESSION['secure'] = genRandomString();
}
$rnd_secure=$_SESSION['secure'];

$AppSecToken="security_token=$rnd_secure&url=$scripturl";

// check content of _REQUEST (the script is called back by the oauth
// server)
if (isset($_REQUEST['enter'])) {
    $enter=$_REQUEST['enter'];
    $AppSecToken="enter=$enter&".$AppSecToken;
}
if (isset($_REQUEST['classpwd'])) {
    $givenpwd=$_REQUEST['classpwd'];
    $AppSecToken="classpwd=$givenpwd&".$AppSecToken;
}
if (isset($_REQUEST['state'])) {
    parse_str($_REQUEST['state'], $vrfystate);
    $enter=$vrfystate['enter'];
    if (isset($vrfystate['classpwd'])) {
        $givenpwd=$vrfystate['classpwd'];
    }
}
if (!isset($enter)) {
    if (isset($_REQUEST['error'])) {
        print "Errore ".$_REQUEST['error']." ".$_REQUEST['error_subtype'];
    } else {
        print "missing class code\n";
    }
    exit();
}
if (isset($_REQUEST['authtype'])) {
    $authtype=$_REQUEST['authtype'];
    $AppSecToken="authtype=$authtype&".$AppSecToken;
}

/**
 * [exec_url description]
 * @param  [type] $url [description]
 * @return [type]      [description]
 */
function exec_url($url)
{
    $file=fopen($url, "r");
    $return="";
    if ($file) {
        $return=fread($file, 2048);
        fclose($file);
    } else {
        $return="WARNING: could not open ".$url;
    }
    return $return;
}

/**
 * [genRandomString description]
 * @return [type] [description]
 */
function genRandomString()
{
    $length = 10;
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $str_len=strlen($characters)-1;
    $string = '';
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, $str_len)];
    }
    return $string;
}

/**
 * [json_decode_nice description]
 * @param  [type]  $json  [description]
 * @param  boolean $assoc [description]
 * @return [type]         [description]
 */
function json_decode_nice($json, $assoc = true)
{
    return json_decode($json, $assoc);
}

/**
 * [crypt_pwd description]
 * @param  [type] $pwd_in [description]
 * @return [type]         [description]
 */
function crypt_pwd($pwd_in)
{
    $pwd_output= "*".crypt($pwd_in, 'Nv0');
    return $pwd_output;
}

include("lang_$lang.php");
?>

<?php
// call back from the oauth server
if (isset($_REQUEST['code'])) :
    // test integrity of the exchange
    $qclass=$vrfystate['enter'];
    if (isset($vrfystate['authtype'])) {
        $authtype=$vrfystate['authtype'];
    } else {
        $authtype="standard";
    }
    if ($vrfystate['security_token']!=$rnd_secure || $vrfystate['url']!=$scripturl) {
        die("errore 1");
    }
    $returncode=$_REQUEST['code'];
    // use curl
    /**
     * (see https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php)
     */
    $data= [
    'code' => $returncode,
                 'client_id' => $client_id,
                 'client_secret' => $client_secret,
                 'redirect_uri' => $scripturl,
                 'grant_type' => 'authorization_code'
    ];

    $datastring=http_build_query($data);

    // open connection
    $ch = curl_init();

    // set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $token_uri);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $datastring);

    // So that curl_exec returns the contents of the cURL; rather than echoing it
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // execute post + json
    $result = json_decode_nice(curl_exec($ch));
    if ($result == false) {
        // Handle error
        die("errore 3");
    } else {
        // got the oauth access_token, get user data

        $access_token=$result['access_token'];
        $data2=array(
            'access_token' => $result['access_token'],
        );
        $url2=http_build_query($data2);

        $optsget = array(
        'http'=>array(
           'method'=>"GET",
        )
        );

        $contextget  = stream_context_create($optsget);

        $userdata=json_decode_nice(file_get_contents($userinfo_uri."?".$url2, false, $contextget));

        // got userdata, we proceed as in ../idp/index.php

        // reset password check
        $vrfypasswd = "NO";

        // $userid must be a uniq identifier from the oauth server (for
        // smartschool is "username" (check userID? see below). It must be
        // mapped to external_auth for adm/raw

        // call authuser (via adm/raw)
        $ctlstr = genRandomString();
        $userid=$userdata['username'];
        $userid=strtolower($userid);

        $url_authuser="$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=authuser&qclass=$qclass&rclass=$rclass&quser=$userid&option=hashlogin";

        $output = json_decode_nice(file_get_contents($url_authuser));

        if (isset($output['status'])
          && ($output['status'] . $output['code'] == "OK$ctlstr")
          && ($authtype != "supervisor")) {
            // user exists in class: enter WIMS
            $script = "window.parent.location='" . $wims . "?session=" . $output['wims_session'] . "'";
            print("<script type=\"text/javascript\">".$script.";</script>");
            exit();
        } else {
            // user does not exists in class, need to do some checking
            //
            // store data returned from adm/raw (we will need hashlogin)
            if (isset($output['message'])) {
                $error=explode(": ", $output['message']);
            }
            // need to check if the oauth server returns user email
            // (Smartschool does not return user email)
            $givenemail="MANCA";
            if (isset($userdata['email'])) {
                $givenemail=$userdata['email'];
            }
            // authtype=supervisor --> check email + enter
            // or
            // user does not exists --> create user

            // NOTE: Smartschool does not return user mail so supervisor login
            // does not work (the routine is here and can be activated for
            // oauth server that return email)
            if ($authtype=="supervisor") {
                // get data from wims
                $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=getclass&qclass=$qclass&rclass=$rclass&option=email";
                $outputtmp = json_decode_nice(file_get_contents($url));
                $supmail=$outputtmp['email'];
                if ($givenemail==$supmail) {
                    $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=authuser&qclass=$qclass&rclass=$rclass&quser=supervisor";
                } else {
                    print "$iniziopagina<br/><br/><br/><strong>ERRORE</strong><br/><br/>you are note the supervisor of $qclass<br/><br/><br/>";
                    print "<pre>";
                    print $outputtmp['message'];
                    print "</pre>";
                    print "<p>torna a <a href=\"$wims\">WIMS</a></p>\n";
                    print "$finepagina";
                    exit();
                }
                $output = json_decode_nice(file_get_contents($url));
                if ($output['status'] . $output['code'] == "OK$ctlstr") {
                    // email check ok: supervisor enter WIMS
                    $script = "window.parent.location='" . $wims . "?session=" . $output['wims_session'] . "'";
                    print("<script type=\"text/javascript\">".$script.";</script>");
                    exit();
                }
            } elseif (($error[0] == "hashlogin")) {
                // $authtype==supervisor
                // wims return valid hashlogin
                //
                // create user
                $logwims = $error[1];
                //
                // do some checking (e.g. class password) before creating user
                // with data from oauth (in $userdata)
                // Note: we do not want to store data (not even in a session) so
                // each time we run the script we call back the oauth
                // server. All data (e.g. the form inputs) need to be stored in
                // "state" => $AppSecToken to get them back to this script.
                // if class type=3 or type=4 show list of subclasses, otherwise
                // ask for class password
                // get class data from WIMS
                $ctlstr = genRandomString();
                $url="$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=getclass&rclass=$rclass&qclass=$qclass&option=supervisor,description,institution,type,classes_list,subclasses_list,password";
                $output = file_get_contents($url);
                $output = utf8_encode($output);
                $output = json_decode($output, true);
                if ($output['status'] . $output['code'] != "OK$ctlstr") {
                    die("errore code");
                }
                //
                // need to check password?
                //
                // Class type 0, 1 or 3 and empty password: no check
                if ($output['password']==""
                 &&
                 ($output['type']=="0"||$output['type']=="1"||$output['type']=="3")
                ) {
                    $vrfypasswd="YES";
                }
                // Class type 0, 1 or 3 or teachers in groups and portals: check
                if ($vrfypasswd=="NO"
                 &&
                 !isset($_REQUEST['classpwd'])
                 &&
                 !isset($vrfystate['classpwd'])
                 &&
                 (
                   ($output['type']=="2"&&$authtype=="teacher")
                   ||
                   ($output['type']=="4"&&$authtype=="teacher")
                   ||
                   $output['type']=="1"
                   ||
                   $output['type']=="3"
                   ||
                   $output['type']=="0"
                 )
                ) {
                    // form to ask user for class password
                    print "$iniziopagina\n";
                    print "<h1>$classpass</h1>\n";
                    print "<form accept-charset=\"utf-8\" action=\"$scripturl\" method=\"post\" target=\"_parent\">\n";
                    print "<input type=\"hidden\" name=\"step\" value=\"step2\">\n";
                    print "<input type=\"password\" name=\"classpwd\" />\n";
                    print "<input type=\"submit\" name=\"iscrizione\" value=\"$enterstring\" />";
                    print "<input type=\"hidden\" name=\"enter\" value=\"$enter\" />\n";
                    if ($authtype == "teacher") {
                        print "<input type=\"hidden\" name=\"authtype\" value=\"teacher\">\n";
                    }
                    print "</form>\n";
                    print "$finepagina\n";
                    exit();
                }
                // groups or portal: no password check,
                //                   prompt user with list of subclasses
                if (($output['type']=="2"&&$authtype!="teacher")
                    ||
                    ($output['type']=="4"&&$authtype!="teacher")
                ) {
                    if ($output['type'] == "4") {
                        $fieldname="classes_list";
                        $numrecord=array(2,3);
                    } else {
                        $fieldname="subclasses_list";
                        $numrecord=array(1,2);
                    }
                    // prompt user with list of subclasses
                    $subclasses=$output[$fieldname];
                    print "$iniziopagina";
                    print "<form action=\"$scripturl\" method=\"post\" target=\"_parent\">\n";
                    print "<label for=\"enter\">$classstr:</label><select name=\"enter\" id=\"enter\">";

                    // fake choice to force student to choose a class in the drop down menu
                    print "<option value=\"NULL\">&nbsp;---&nbsp;$choose&nbsp;---&nbsp;</option>";
                    foreach ($subclasses as $index => $values) {
                        // set variable for qclass
                        $tmpqclass=str_replace("/", "_", $subclasses[$index]['0']);
                        if ($output['type'] == "4") {
                            $tmpqclass=$qclass."_".$tmpqclass;
                        }

                        print "\n<option value=\"" . $tmpqclass . "\">" . str_replace("\r\n", "", $subclasses[$index][$numrecord[0]] . " (" . $subclasses[$index][$numrecord[1]] . ")") . "</option>";
                    }
                    print "\n</select>\n";
                    print "<input type=\"hidden\" name=\"step\" value=\"step2\">\n";
                    print "<input class=\"wims_button\"
                      style=\"color:red$button_color;\"
                      type=\"submit\"
                      name=\"iscrizione\"
                      value=\"$enterstring\"
                      >\n";
                    print "</form>\n";
                    print "$finepagina\n";
                    exit();
                }
                if (isset($vrfystate['classpwd'])&&$vrfystate['classpwd']!="") {
                    $cryptgivenpwd = "*" . crypt($givenpwd, 'Nv0');

                    // check that the passord typed correspond to the class password
                    //
                    // standalone class (type=0)
                    if ($output['type']=="0"&&$output['password']==$cryptgivenpwd) {
                        $vrfypasswd="YES";
                    } elseif ($output['type']=="1"&&$output['password']==$cryptgivenpwd) {
                        // subclass in a group
                        $vrfypasswd="YES";
                    } elseif ($output['type']=="3"&&$output['password']==$cryptgivenpwd) {
                        // subclass in a portal
                        $vrfypasswd="YES";
                    } elseif ($authtype=="teacher"&&($output['type']=="2"||$output['type']=="4")&&$output['password']==$cryptgivenpwd) {
                    // teacher in class or portal
                        $vrfypasswd="YES";
                    } else {
                        print "$iniziopagina";
                        print "Wrong password\n";
                        print "$finepagina";
                        die();
                    }
                }
                if ($vrfypasswd=="YES") {
                    // user data are in $userdata
                    //
                    // see https://www.smartschool.be/fr/oauth/#toggle-id-13
                    //
                    //    [userID] => LfqTbkq16hR8yRuhBeaZHA==
                    //    [name] => Piet
                    //    [surname] =>  Peters
                    //    [fullname] =>  Peters Piet
                    //    [username] =>  petersp
                    //    [platform] =>  https:\/\/subdomain.smartschool.be
                    //    [isMainAccount] =>  1
                    //    [isCoAccount] =>  0
                    //    [nrCoAccount] =>  0
                    //    [typeCoAccount] =>
                    //    [actualUserName] =>  Piet
                    //    [actualUserSurname] =>  Peters
                    //
                    $adduser="firstname=".$userdata['name']."
            lastname=".$userdata['surname'];

                    $pwd = genRandomString();
                    $pwd = "*" . crypt($pwd, 'Nv0');
                    $adduser = $adduser . "
            password=$pwd\nexternal_auth=$userid\nagreecgu=no";

                    if ($authtype=="teacher") {
                        $adduser = $adduser . "\nsupervisable=yes";
                    }

                    $adduser = urlencode(utf8_decode($adduser));

                    $ctlstr = genRandomString();
                    $url="$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=adduser&qclass=$qclass&rclass=$rclass&quser=$logwims&data1=$adduser";
                    $output = json_decode_nice(file_get_contents($url));
                    // verify user has been created
                    if ($output['status'] . $output['code'] == "OK$ctlstr") {
                        $user_id=$output['user_id'];
                        // open an authenticated session
                        $ctlstr = genRandomString();
                        // we have userid as we just created the user
                        $url = "$lwims?module=adm/raw&ident=$ident&lang=$lang&passwd=$identpwd&code=$ctlstr&job=authuser&qclass=$qclass&rclass=$rclass&quser=$user_id";
                        $output = json_decode_nice(file_get_contents($url));
                        if ($output['status'] . $output['code'] == "OK$ctlstr") {
                            // enter WIMS
                            $script = "window.parent.location='" . $wims . "?session=" . $output['wims_session'] . "';";
                            print("<script type=\"text/javascript\">".$script."</script>");
                            exit();
                        } else {
                            // could not open the session
                            print "$iniziopagina\n$textindex\n";
                            print "$entererrormsg\n<pre>\n";
                            print $output['status'] . ": " . $output['message'];
                            print "\n</pre>\n";
                            switch (substr($output['message'], 0, 30)) {
                                case "modification of class not allowed":
                                case "modification of class not allo":
                                case "connection refused by requeste":
                                    print "<p>$connectrefusedmsg</p>\n";
                                    break;
                                default:
                                    echo "<p>$defaulterrormsg</p>\n";
                            }
                            print "$finepagina\n";
                            exit();
                        }
                    } else {
                        // could not create user
                        print "$iniziopagina\n$textindex\n";
                        print "$entererrormsg\n<pre>\n";
                        print $output['status'] . ": " . $output['message'];
                        print "\n</pre>\n";
                        switch (substr($output['message'], 0, 30)) {
                            case "modification of class not allowed":
                            case "modification of class not allo":
                            case "connection refused by requeste":
                                print "<p>$connectrefusedmsg</p>\n";
                                break;

                            default:
                                echo "<p>$defaulterrormsg</p>\n";
                        }
                        print "$finepagina\n";
                        exit();
                    }
                }
            }
        }
    }
else :
    /*
      first access of the script and return after password check
    */
    if (isset($_REQUEST['classpwd'])||isset($vrfystate['classpwd'])||(isset($_REQUEST['step'])&&$_REQUEST['step']=='step2')) {
        // TODO: setup routines (e.g. use refresh_token) so to avoid to
        // prompt again the user with login form
        $data1= array(
            // https://www.smartschool.be/fr/oauth/#toggle-id-8
            //
            'client_id' => $client_id,
            'response_type' => 'code',
            'scope' => 'userinfo',
            'redirect_uri' => $scripturl,
            'state' => $AppSecToken,
        );
        $url1=http_build_query($data1);
        $script = "window.parent.location='".$auth_uri."?".$url1."'";
        print("<script>".$script.";</script>");
        exit();
    } else {
        // executed on the first run of the script: call the oauth server
        $data1= array(
            'client_id' => $client_id,
            'response_type' => 'code',
            'scope' => 'userinfo',
            'redirect_uri' => $scripturl,
            'state' => $AppSecToken,
        );
    }
    $url1=http_build_query($data1);
    $script = "window.parent.location='".$auth_uri."?".$url1."'";
    print("<script>".$script.";</script>");
    exit();
endif;

print "<p>check class connections</p>\n";
