# phpOAuth
In this directory you find examples of php scripts to access WIMS
classes with OAuth authentification.

Please note that the lang files from `../idp` are used and should be
copied in the same directory with these files.

Requirements: `php_curl`

## Gmail

[todo]

## Smartschool

We assume that the smartschool.php script is installed at the root of the WIMS server, i.e. it is accessible at the url `https://_address_of_wims_server_/smartschool.php`.
The variable `$scripturl` in the php file must point to the script file.

Customize the variables at the top of the file smartschool.php
(according also to instruction further down in this file).

### Smartschool setup

See [www.smartschool.be/fr/oauth/](https://www.smartschool.be/fr/oauth/)

Need to fill in the form at the end of the page inserting the address
of the script as the "Redirect URI de votre application".

When you get your clientID and secretID from Smartschool edit
`cliend_id` and `client_secret` in the script file.

#### Privacy

The script only requires `userinfo` scope (see
[www.smartschool.be/fr/oauth/](https://www.smartschool.be/fr/oauth/#toggle-id-13)) and in particular it only relies on:
	
	"name": "Piet",
	"surname": "Peters",
	"username": "petersp",

### WIMS setup

Need to configure a connection (`log/classes/.connections`) and edit
`ident` and `identpwd` in the script file (e.g. you can use the file
`phpidp` from `../idp`).

### Class setup

In order to use this authentification, classes should have these
variables set:

	!set class_authidp=php;php
	!set class_php_auth=https://_address_of_wims_server_/smartschool.php
	!set class_connections=+phpidp/smart+
