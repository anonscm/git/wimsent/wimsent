<!--
author: Pierre PÉRONNET <pierre.peronnet@gmail.com>
-->

Application de test
===================

Tester un Tool Provider
-----------------------

Voici le lien de l'application permettant de tester un *TP* : <http://ltiapps.net/test/tc.php>.

### Les étapes

 1. Créer du contenu sur le *TP*.
    Pour que l'application puisse tester votre *TP*, il faut que ce dernier possède au moins un utilisateur ayant accès à une ressource.

 2. Saisir la clef du *TP*.
![Options d'enregistrement d'un TP](./images/app_test_tc_registration_settings.png "Options d'enregistrement")
    * Launch URL : l'url vers votre *TP* http://your-/Users/pierre/OneDrive/CoffeePot/lti/lti/descriptif_protocole.md
tool-provider.com.
    * Consumer Key : 
    * Shared secret : La clef générée par votre *TP*.

 3. Choisissez la requête à envoyer
![Déroulement général du processus](./images/app_test_tc_message_data.png "Données du messages")
    * Le premier message à tester sera sans doute le `basic-lti-launch-request`.
    * Viendra ensuite les autres requêtes `ContentItemSelectionRequest`, `DashboardRequest` et `ConfigureLaunchRequest`

 4. Vous pouvez visualizer les requêtes envoyées et reçues en appuyant sur le bouton *View request*.

Afin de tester le *Tool Provider*, **il est possible d'utiliser des données pré-enregistrées**. Pour cela il faut sélectionner le *Tool Consumer* dans le champs *Reset sample data to ...*.

Seuls les champs notés `[R]` sont requis. Les champs notés `[r]`sont recommandés.
Il est aussi possible d'afficher les champs optionnels en cochant la case *Display optional parameters?*

### Les données de la ressource

Ces informations servent à identifier la ressource demandées par le *Tool Consumer* à votre *Tool Provider*.

![Données de la ressource lors d'un requête](./images/app_test_tc_context_ressource_data.png "Données de la ressource")

### Les données du *TC*

Ces données permettent de fournir des informations à propos du *TC*.

![Données du Tool Consumer lors d'un requête](./images/app_test_tc_tool_consumer_data.png "Données du TC")

### Les données utilisateur

Ces champs permettent de détailler l'utilisateur du *TC* lors de la requête.

![Données de l'utilisateur lors d'une requête](./images/app_test_tc_user_data.png "Données de l'utilisateur")

### Les données du message (uniquement pour la requête `ContentItemSelectionRequest`)

La description de ces données arrivera bientôt...

![Données de message lors d'une requête](./images/app_test_tc_content_item_message_data.png "Données de message")

Tester un Tool Consumer
-----------------------

Lien vers l'application de test : <http://ltiapps.net/test/tp.php>.

### Les étapes



