<!--
author: Pierre PÉRONNET <pierre.peronnet@gmail.com>
-->

Description du protocole OAuth
==============================

Auteur : Pierre PÉRONNET <pierre.peronnet@gmail.com>

Mécanisme de sécurité pour les requêtes *POST* et *GET*.

Les requêtes LTI à protéger sont les requêtes **launch** ainsi que **toutes les requêtes serializées**.

OAuth 1.0 spécifie la façon de **construire** un message de base puis la **signature** de cette chaîne.

Les TP doivent obligatoirement supporter la méthode **HMAC-SHA1**.

Un secret **oauth\_consumer\_secret** doit être enregistré par le TC :

 - Soit pour chaque **guid** d'instance de TP (par domain).
 - Soit pour chaque **lien** de TP.

### Paramètres nécessaires

 - **oauth\_consumer\_key**=*\<key\>*
 - **oauth\_signature\_method**=*\<method_name\>*
 - **oauth\_timestamp**=*\<timestamp\>*
 - **oauth\_nonce**=*\<nonce\>*
 - **oauth\_version**=1.0
 - **oauth\_signature**=*\<signature\>*
 - **oauth\_callback**=*\<url | about:blank\>*

Les TP doivent vérifier que le timestamp soit récent.

 1. Quelques **minutes** **sans** vérification des **nonces**.
 2. Quelques **heures** **avec** vérification des **nonces**.

Si le timestamp est valide, il faut ensuite vérifier le *nonce* pour qu'une requête avec un nonce spécific ne puisse être executée qu'une seule fois.




