<!--
author: Pierre PÉRONNET <pierre.peronnet@gmail.com>
-->

Tool Consumer Profile
=====================

### Requêter un profile

Requête *GET* vers l'url spécifié dans la requête *launch* à laquelle il faut ajouter le paramètre `lti_version`. 

```HTTP
GET relative/url?lti_version=LTI-1p2 HTTP/1.0
Host: ims.domain.com
Accept: application/vnd.ims.lti.v2.toolconsumerprofile+json
```

### Renvoyer le profile

Après la requête précédente, le TP peut envoyer sa réponse :

```HTTP
HTTP/1.0 200 OK
Date: Mon, 4 Jan 2016 12:34:56 GMT
Content-Type: application/vnd.ims.lti.v2.toolconsumerprofile
Content-Length: taille
Expires: Mon, 4 Jan 2016 12:34:56 GMT
```

Le contenu de la réponse est au format *JSON* et contient de nombreuses données.

#### Données requises

```json
{
    "@context" : [
        "http://"
    ],
    "@type" : "ToolConsumerProfile",
    "lti_version" : "LTI-1p0",
    "guid" : "<consumer_guid>",
    "product_instance" : {
        "guid" : "<product_guid>",
        "product_info" : {
            "product_name" : {
                "default_name" : "<product_default_name>"
            },
            "product_version" : "<product_version>",
            "product_family" : {
                "code" : "<product_family_code>",
                "vendor" : {
                    "code" : "<domain.com>",
                    "vendor_name" : {
                        "default_value" : "<LMS Corporation>",
                        "key" : "<product.vendor.name>"
                    },
                    "timestamp" : "<2016-01-4BT09:08:16-04:00>"
                }
            }
        }
    },
    "capability_offered" : [
        "basic-lti-launch-request"
    ]
}
```


