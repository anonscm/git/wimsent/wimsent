<!--
author: Pierre PÉRONNET <pierre.peronnet@gmail.com>
-->

Implémentation du protocole LTI
===============================

Liste des fichiers
------------------

 * Fichier [cahier_des_charges](pdf/cahier_des_charges.pdf) - Cahier des charges de la mission.
 * Fichier [descriptif_protocole](pdf/descriptif_protocole.pdf) - Descriptif générale du protocole *LTI*. Certainement le premier document à lire après le cahier des charges.
 * Fichier [applications_test](pdf/applications_test.pdf) - Descriptif du fonctionnement des applications de test du protocole *LTI*.
 * Fichier [descriptif_oauth](pdf/descriptif_oauth.pdf) - Descriptif du protocole OAuth 1 utilisé pour chiffré les messages *LTI*.
 * Fichier [lti_load](pdf/lti_load.pdf) - Descriptif de la charge supplémentaire apportée par le protocole *LTI*.
 * Fichier [tool_consumer_profile](pdf/tool_consumer_profile.pdf) - Descriptif des requêtes permettant la découverte de profile des *TC*.


