#!/usr/bin/env perl

use strict;
use warnings;

#use FindBin;               # locate this script
#use lib "$FindBin::Bin/";  # use the parent directory

# Net::OAuth library must be installed (cpan install Net::OAuth)
use Net::OAuth;
$Net::OAuth::PROTOCOL_VERSION = Net::OAuth::PROTOCOL_VERSION_1_0A;

# On recupere les parametres fournis par WIMS
my $consumer_key='key';my $consumer_secret='secret';my $expected_url='http://localhost/wims/';
push (@ARGV,split(' ', $ENV{'wims_exec_parm'})) if ($ENV{'wims_exec_parm'});
while ($_ = shift (@ARGV))
{
  last if (!/^--/);
     if (/^--key=(.*)$/) { $consumer_key          = $1; } # consumer_key
     if (/^--secret=(.*)$/) { $consumer_secret    = $1; } # consumer_secret
     if (/^--expected_url=(.*)$/) { $expected_url = $1; } # expected_url
};

# ainsi que les parametres directement accessibles par l'environnement
my $request_method='GET';
if ($ENV{'REQUEST_METHOD'}){
    $request_method=$ENV{'REQUEST_METHOD'};
};

=DEBUG
# DEBUG : Generating an oAUTH Signature
my $attended_request = Net::OAuth->request('RequestToken')->new(
        consumer_key => $consumer_key,
        consumer_secret => $consumer_secret,
        request_url => $expected_url,
        request_method => $request_method,
        signature_method => 'HMAC-SHA1',
        timestamp => '1191242096',
        nonce => 'kllo9940pd9333jh',
        callback => 'about:blank',
);
$attended_request->sign;
my $signature = $attended_request->signature;

print "\n oauth_signature = $signature\n";
# Signature (GET) = uRc38XiLHeFqtLvAarTv6Ay8k+k=
# Signature (POST)= EP/5LFwzlKbvhBVZIQY06TxGf20=
=cut

my %api_params= (request_method => $request_method,
     request_url => $expected_url,
     consumer_secret => $consumer_secret);

my $request_url="?".$ENV{'QUERY_STRING'};
#=DEBUG
# my $received_request = Net::OAuth->request('consumer')->from_authorization_header($ENV{HTTP_AUTHORIZATION}, %api_params);
my $received_request = Net::OAuth->request('RequestToken')->from_url($request_url, %api_params);
# my $received_request = Net::OAuth->request('consumer')->from_hash({$q->Vars},%api_params); # CGI

if ($received_request->verify){
  print "OK\n** oAUTH pass, You can continue !\n";
}else{
  print "ERROR\n** Sorry, connexion rejected by oAuth !\n";
}
#=cut