# WIMS LTI Connector


To declare an autorized LTI Consumer, add a file in `log/classes/.lti_connections` folder

The name of the file will be the `consumer_key`

And in this file, add theses params :

* OAUTH_SECRET = secret (a secret password only known by the lti consumer and your server)
* ...


Then, just call this module from your LTI consumer : [http://your_wims_server/wims/LANG_adm~lti.html]()
(replace (LANG) by your prefered language code)

Here is a sample on localhost with french interface : [localhost/wims/fr_adm~lti.html](http://localhost/wims/fr_adm~lti.html)
