<!--
author: Pierre PÉRONNET <pierre.peronnet@gmail.com>
-->

Montée en charge
================

Introduction
------------

Ce document présente une étude de la montée en charge ajoutée lors de l'implémentation du protocole *LTI*.

### Le *Tool Provider*

La charge du *Tool Provider* est dépendante des réglages du *Tool Consumer*.

#### Nombre de requête

En fonction de l'impémentation du *Tool Consumer*, le *Tool Provider* peut voir le nombre de requête quasiment inchangé.

Le *Tool Consumer* peut agir de différentes manières pour récupérer un exercice (voir `launch_presentation_document_target`). Ainsi dans le cas d'une intégration `embed`, le *TC* choisi les intéractions disponibles pour l'utilisateur, et ainsi augmenter le nombre de requête du *TP*. Dans les autres cas, l'utilisateur intérargit directement avec le *TP* et donc il est le principal responsable de sa montée en charge.

En utilisation normale, le *Tool Consumer* augmente sa charge car le nombre de ressoures augmente cependant cette augmentation est linéaire.

De plus, la charge du *Tool Provider* reste inchangée modulo le nombre de répétition d'un exercice:

#### Répétition de requêtes

Certains *CMS* permettent d'accéder plusieurs fois à la même ressource, par exemple plusieurs tentatives d'un exercice. Dans le cas où ce CMS sert de *Tool Provider*, il peut informer le *Tool Consumer* des essais réalisés et ainsi renvoyer la main au *TC* à chaque tentative. Ce procédé permet de garder le contrôle des accès aux différentes ressources du *TP*. Mais risque également d'augmenter la charge de ce dernier.

### Conclusion

La différence de charge dépendra notamment de l'implémentation de l'appel du *Tool Provider* dans le *Tool Consumer*.

