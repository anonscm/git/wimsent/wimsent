<!--
author: Pierre PÉRONNET <pierre.peronnet@gmail.com>
-->

Description de l'interface du Tool Consumer
===========================================

Auteur : Pierre PÉRONNET <pierre.peronnet@gmail.com>

Quelques librairies sont référencées sur [le site d'IMS Global](http://www.imsglobal.org/learning-tools-interoperability-sample-code), mais aucune n'est écrite en `C`.
Je propose donc [ces modules](./src/lib) pour implémenter le protocole LTI dans *WIMS*.


