#ifndef _LTI_COMMON_
#define _LTI_COMMON_

#include <stdbool.h>
#include <time.h>

#include "lti_version.h"

/**
 * Type that represent scope.
 */
typedef enum {
    ID_ONLY,  //!< Use ID value only.
    GLOBAL,   //!< Prefix an ID with the consumer key.
    CONTEXT,  //!< Prefix the ID with the consumer key and context ID.
    RESOURCE //!< Prefix the ID with the consumer key and resource ID.
} lti_scope;


/**
 * Structure to represent an LTI Tool Provider
 */
typedef struct _lti_tool_provider lti_tp;

/**
 * Structure to represent a tool consumer
 */
typedef struct _lti_tool_consumer lti_tc;

/**
 * Generate a web page containing an auto-submitted form of parameters.
 * @param url    - URL to which the form should be submitted
 * @param form_keys - form parameters keys associated with form_values
 * @param form_values - form parameters values associated with form_keys
 * @param target - Name of target
 */
void sendForm(char* url, char** form_keys, char** form_values, char* target);

#endif
