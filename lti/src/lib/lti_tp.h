#ifndef _LTI_TOOL_PROVIDER_
#define _LTI_TOOL_PROVIDER_

#include <stdbool.h>

#include "lti_common.h"
#include "lti_request.h"


struct _lti_tool_provider {
};


/**
 * Create a response to a launch request.
 */
lti_req* lti_tp_create_launch_response(char* return_url, bool success, char* message, lti_message_level* level);

/**
 * Tool consumer which sent the request.
 * @param req - Request received.
 * @return lti_tc* - Tool consumer sender or NULL on failure.
 */
lti_tc* lti_tp_get_consumer(lti_req* req);

/**
 * Whether shared resource link arrangements are permitted.
 * @param tp - Tool provider which will execute request.
 * @return bool - True if allowed.
 */
bool lti_tp_is_sharing_allowed(lti_tp* tp);

/**
 * Is the consumer key available to accept launch requests ?
 * @param tc - Tool Consumer to check
 * @return bool - True if the consumer key is enabled and within any date constraints
 */
bool lti_tp_is_consumer_available(lti_tc* tc);

/**
 * Default email domain.
 * @param tp - Tool provider which will execute request.
 * @return char* - email.
 */
char* lti_tp_default_email(lti_tp* tp);

/**
 * Process an incoming request.
 * @param tp - Tool provider which will handle request.
 * @return lti_req* - Get next message or NULL if failure.
 */
lti_req* lti_tp_handle_request(lti_tp* tp);

/**
 * Add a parameter constraint to be checked on launch.
 * @param name          - Name of parameter to be checked
 * @param required      - True if parameter is required
 * @param max_length    - Maximum permitted length of parameter value.
 * @param message_types - Array of message types to which the constraint applies (default is all)
 */
void lti_tp_set_parameter_constraint(char* name, bool required, int max_length, lti_req_type* message_types);

/**
 * Add a parameter to a request.
 * @param req - Request to change.
 * @param name - Parameter name.
 * @param value - Parameter value.
 */
void lti_tp_add_request_parameter(lti_req* req, char* name, char *value);

#endif
