#ifndef _LTI_VERSION_
#define _LTI_VERSION_

/**
 * Type that represent the version of LTI.
 */
typedef char* lti_version;

/**
 * LTI version 1 for messages.
 */
#define LTI_VERSION1 "LTI-1p0"

/**
 * LTI version 2 for messages.
 */
#define LTI_VERSION2 "LTI-2p0"

/**
 * Get value of the version. Ready to send to consumer and provider.
 * @warning Do not forget to free name after use.
 * @param version - Version to get the value
 * @return char* - Value of the version
 */
char* lti_version_value(lti_version version);

/**
 * Get name of the version. Ready to display to user.
 * @warning Do not forget to free name after use.
 * @param version - Version to display
 * @return char* - Displayable string of the version name.
 */
char* lti_version_display_name(lti_version version);

#endif
