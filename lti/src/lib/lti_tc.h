#ifndef _LTI_TOOL_CONSUMER_
#define _LTI_TOOL_CONSUMER_

#include <stdbool.h>

#include "lti_common.h"
#include "lti_request.h"

struct _lti_tool_consumer_data;

struct _lti_tool_consumer {
	char* secret;            //!< Shared secret.
	char* local_name;        //!< Local name of tool consumer.
	char* default_email;     //!< Default email address (or email domain) to use when no email address is provided for a user.
	char* css_path;          //!< Optional CSS path (as reported by last tool consumer connection).
	char* version;           //!< Tool consumer version (as reported by last tool consumer connection).
	char* name;              //!< Name of tool consumer (as reported by last tool consumer connection).
	char* guid;              //!< Tool consumer GUID (as reported by first tool consumer connection).
	char* key;               //!< Get consumer key.
	time_t* created;         //!< Date/time when the object was created.
	time_t* updated;         //!< Last time the object have been updated.
	time_t* last_access;     //!< Date of last connection from this tool consumer.
	time_t* enable_until;    //!< Date/time until which the tool consumer instance is enabled to accept incoming connection requests.
	time_t* enable_from;     //!< Date/time from which the the tool consumer instance is enabled to accept incoming connection requests.
	lti_scope default_scope; //!< Default scope to use when generating an Id value for a user.

	struct _lti_tool_consumer_data* data; //!< More datas
};


/**
 * Tool provider which sent the request.
 * @param req - Request received.
 * @return lti_tp* - Tool provider sender or NULL on failure.
 */
lti_tp* lti_tc_get_provider(lti_req *req);

/**
 * Check if request is a success.
 * @param req - Request received.
 * @return bool - True if request succeed.
 */
bool lti_tc_request_succeed(lti_req *req);

/**
 * Intialize consumer with value from a database.
 * @param tc   - Tool Consumer to intialize
 * @param data - Tool Consumer to check
 * @return bool - True if the consumer key is enabled and within any date constraints
 */
bool lti_tc_intialize(lti_tc* tc, void *data);

/**
 * Add the OAuth signature to an LTI message.
 * @param tc      - Tool Consumer used to sign message.
 * @param url     - URL for message request
 * @param type    - LTI type message
 * @param version - LTI Version
 * @param params - Message parameters
 */
void lti_tc_sign_parameters(lti_tc* tc, char* url, lti_req_type type, char* version, lti_req_parameters* params);

/**
 * Whether the tool consumer instance is enabled to accept incoming connection requests.
 * @param tc   - Tool Consumer to check.
 * @return bool - True if enabled
 */
bool lti_yc_is_enabled(lti_tc* tc);

/**
 * Whether the tool consumer instance is protected by matching the consumer_guid value in incoming requests.
 * @param tc   - Tool Consumer to check.
 * @return bool - True if protected
 */
bool lti_tc_is_protected(lti_tc* tc);

#endif
