#ifndef _LTI_REQUEST_
#define _LTI_REQUEST_

#include <stdbool.h>

#include "lti_common.h"

/**
 * Type of LTI request.
 */
typedef enum {
	LAUNCH
} lti_req_type;

/**
 * Encoding methods.
 */
typedef enum {
	HMAC_SHA1 //!< HMAC-SHA1. Should be the defaut method.
} lti_req_encode_method;

/**
 * Level of message in LTI request.
 */
typedef enum {
	MDG_DISPLAY,   //!< Success message which should be displayed to end user.
	MSG_LOG,       //!< Success message to log.
	ERROR_DISPLAY, //!< Error message which should be displayed to end user.
	ERROR_LOG      //!< Error message to log.
} lti_message_level;

typedef struct _lti_req_parameters {

} lti_req_parameters;

/**
 * LTI request.
 */
typedef struct _lti_request {
} lti_req;

/**
 * Character used to separate each element of an ID.
 */
#define LTI_ID_SCOPE_SEPARATOR ':'

/**
 * 
 */
void lti_req_destroy(lti_req* req);

/**
 * Get an array of fully qualified user roles.
 * @param req - Request sent.
 * @return char** - Array of roles.
 */
char** lti_req_get_roles(lti_req* req);

/**
 * Get an array of fully qualified user roles.
 * @param req - Request sent.
 * @return char** - Array of roles.
 */
lti_req_type* lti_req_get_type(lti_req* req);

/**
 * Get the url to return.
 * @param req - Request sent.
 * @return char* - Return url.
 */
char* lti_req_get_url_return(lti_req* req);

/**
 * Get value of the version. Ready to send to consumer and provider.
 * @param req - Request sent.
 * @return lti_version - Version corresponding to the value.
 */
lti_version lti_req_get_version(lti_req* req);

/**
 * Encode request in order to send it.
 * @param req - Request to encode.
 * @param method - Method used to encode req.
 * @return char* - Encoded request.
 */
char* lti_req_encode(lti_req* req, lti_req_encode_method method);

#endif
