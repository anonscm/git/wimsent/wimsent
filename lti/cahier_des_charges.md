<!--
author: Pierre PÉRONNET <pierre.peronnet@gmail.com>
-->

Wims - Intégration LTI
======================

Document de référence - Cahier des charges
------------------------------------------

### Présentation

Le protocole *LTI* permet la communication entre deux plateformes pédagogiques, des *LMS*. Ainsi une plateforme peut partager des informations concernant ses exercices à une seconde. Une plateforme peut donc délégué la gestion d'exercices à une autre (affichage, notation, ...).

#### Présentation de *WIMS*

*[WIMS]("http://wims.unice.fr/wims")* (WWW Interactive Multipurpose Server) est un *LMS*. Il permet de consulter des cours interactifs, répondre à des exercices mathématiques, faire des calculs et tracer courbes ou surfaces, ou jouer aux jeux mathématiques, en cliquant sur des boutons ou des dessins comme sur toute autre page web.

Chaque intéraction envoie une *requête* au serveur, qui la traite et retourne le résultat. Quelquefois, du javascript et/ou des applets java sont aussi utilisés pour renforcer cette interactivité.

La structure de WIMS est particulièrement intéressante pour les activités d'enseignement, dans lesquelles le serveur peut analyser individuellement le comportement d'étudiants/élèves, et proposer des activités à chacun d'eux suivant son besoin d'apprentissage.

### Objectifs du projet

Je désirerai que ce protocole soit intégré à *WIMS*. Cela ne devrait pas dépendre de l'autre plateforme (c'est le principe des communications standardisées).

Nous ne demandons pas l'implémentation dans wims à priori mais simplement les principes et éléments de code pour pouvoir le faire.

 - Les interfaces nécessaire pour le fonctionnement du protocole *LTI*.
 - Architecture logique de composants *WIMS* à développer.
 - Évaluation de la charge qui sera ajoutée au serveur *WIMS*.
 - Une aide lors de la phase de debuggage ainsi qu'un outil permettant de tester l'implémentation du protocole.

Moodle intégre déjà ce [protocole](https://docs.moodle.org/en/LTI_Provider). Ce n'est donc pas un développement dans Moodle que j'ai en vue, l'intérêt du protocole étant qu'il est **universel**.

Le processus est symétrique, cela signifie qu'il faut pouvoir envisager que WIMS récupère des notes ou des informations d'une autre plateforme.

### Planning

Au niveaau deadLine et planning. Je propose que une sorte de standup meeting (une réunion pour faire le point) toutes les deux semaines pendant le mois de décembre. Puis un entretien *régulier* jusqu'à la livraison de la première version des livrables courant janvier.
Je prévois l'application de test **fin janvier** pour la **première version**.
Les documents seront mis à jour sur le dépôt **WimsEnt** de [SourceSup](https://sourcesup.renater.fr/projects/wimsent/).

### Cas d'utilisations

Un enseignant (logué sur son LMS) souhaite intégrer un exercice WIMS dans son cours

 - il choisit l'activité grâce au moteur de recherche ou en donnant sa référence.
 - il choisit les paramètres.
 - il installe l'activité dans son cours.

L'étudiant

 - se connecte à la plateforme.
 - entre dans le cours du prof sur le LMS.
 - clique sur l'exo et est envoyé dans WIMS.
 - l'Ètudiant obtient une note.
 - le LMS obtient alors :
   - les notes de l'exercice fait (si répété).
   - le nombre de tentatives.
   - le temps passé.

Un scénario symétrique est aussi à envisager :
un enseignant logué sur wims désire utiliser un exercice sur une autre *LMS* donnant la possibilité du protocole LTI et récupérer la note.

### Travaux attendus

Cette section décrit les attentes du projet: 2 livrables et un développement.

#### Livrable: Documentation de l'interface et des opérations (formules)

L'interface qui sera rédigée comprendra:

 - Les définitions des points d'entrés du programme pour le protocole :
   - Spécification des paramètres recu par le programme.
   - Spécification de la réponse renvoyé par le programme.
   - Description de l'objectif de la fonction.
 - La description du processus dans le protocole LTI (Ordre des points d'entrés à utiliser).

#### Livrable: Documentation Oauth 1

##### Présentation de *OAuth 1*

Le protocole LTI s'appuie sur un protocole d'authentification : *OAuth 1*.

> OAuth est un protocole libre, créé par Blaine Cook et Chris Messina.
> Il permet d'autoriser un site web, un logiciel ou une application.
> (dit "consommateur") à utiliser l'API sécurisée d'un autre site web.
> (dit "fournisseur") pour le compte d'un utilisateur.
> OAuth n'est pas un protocole d'authentification, mais de "délégation d'autorisation".

*Source: [Wikipedia](https://fr.wikipedia.org/wiki/OAuth)*

##### Présentation du livrable

Le premier travail serait de nous indiquer comment fonctionne *OAuth1* et comment l'intégrer à WIMS (Un petit mot sur la sécurité du protocole serait aussi intéressant).

Si je compare avec l'authentification *CAS*, pour implémenter, j'ai eu besoin de connaitre les requêtes url :

 - comme la requête à envoyer *login?service=*
 - puis comment récupérer le ticket *validate?ticket=xxx&service=yyy*.
 - etc...

Je n'ai bien sûr pas eu besoin d'installer un serveur *CAS*.

#### Développement et Support: Debuggage

Fabriquer un mini-interlocuteur (comme l'a fait Olivier pour le module raw, qui peut être en php). je ne veux pas quand à moi tester à partir de Moodle.
Aider à débuguer. cela risque d'être plus tard dans un deuxième temps.

Nous ne désirons pas utiliser php dans wims pour l'instant.
Par contre, j'avais vu qu'il y avait des paquets perl concernant *OAuth1*. Comme perl est déjà utilisé, si cela simplifie beaucoup cela peut être utilisé. Des scripts "standard" seraient alors fournis et expliqués.

<http://www.imsglobal.org/lti/>
<http://latlntic.unige.ch/ntic/projets/files/other/IMS-LTI.pdf>
<http://www.edugarage.com/download/attachments/75071496/2010-06-15-blti.pdf>

#### Évaluation de la charge

En implémentant le protocole LTI à WIMS, ce dernier va voir son traffic augmenté. Il faut donc faire une évaluation du nombre de requêtes effectuées en fonction du nombre d'utilisateur et du nombre d'exercices.


