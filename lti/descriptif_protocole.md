<!--
author: Pierre PÉRONNET <pierre.peronnet@gmail.com>
-->

Description du protocole LTI
============================

Introduction
------------

Un *Tool Consumer* peut ajouter un *Tool Provider* dés lors qu'il connait sa clef secrète.

![Déroulement général du processus](./images/deroulement_general.png "Déroulement général")

### Les manipulations à faire avant qu'une ressource soit accessible

Avant d'envoyer que les visiteurs puissent accéder à une ressource (requête *launch*), il est nécessaire de partager des informations entre le *Tool Provider* et le *Tool Consumer*.

 1. Échange de clef.
    1. Le *TP* doit générer une clef.
    2. Le *TC* doit ajouter le *TP* à partir de son nom de domaine et sa clef.
 2. Ajout d'une ressource.
    1. Le *TP* doit posséder une ressource. 
    2. Le *TC* enregistre l'url de la requête *launch*. 

### Requête *Launch*

Le *TC* doit fournir autant d'informations que possible au *TP* lors du lancement.

Les *TP* doivent être prêt à travailler avec des informations partielles car le *TC* peut se réserver le droit de ne pas les fournir.

#### Informations à transmettre 

Les informations sont transmises avec la méthode *POST*.

##### Informations requises

 - `lti_message_type`=basic-lti-launch-request
 - `lti_version`=LTI-1p0
 - `ressource_link_id`= *\<ID\>*

##### Informations recommandées

 - `ressource_link_title`= *\<Title\>*
 - `user_id`= *\<User_ID\>*
 - `roles`= *\<Role\_1\>,\<Role\_2\>,...* (see [Learning IS](LIS))
 - `context_id`= *\<Context_ID\>*
 - `context_title`= *\<Title\>*
 - `context_label`= *\<Label\>* (short title)
 - `launch_presentation_document_target`= *\<frame | iframe | window | popup | overlay | embed\>*
 - `launch_presentation_width`= *Integer:Number\_of\_pixel*
 - `launch_presentation_height`= *Integer:Number\_of\_pixel*
 - `launch_presentation_return_url`= *\<Url\>*
 - `tool_consumer_info_product_family_code`= *\<\>*
 - `tool_consumer_info_version`= *\<Major\_version\_number\>–\<Minor_version\>*
 - `tool_consumer_instance_guid`= *\<TC_ID\>*
 - `tool_consumer_instance_name`= *\<TC_label\>*
 - `tool_consumer_instance_contact_email`= *\<TC\_sysadmin\_email\>*

Les informations suivantes sont recommandées mais peuvent être omises pour des raisons de **confidentialité**.

 - `lis_person_name_given`= *\<Prénom\>*
 - `lis_person_name_family`= *\<Nom\>*
 - `lis_person_name_full`= *\<Nom complet\>*
 - `lis_person_name_email_primary`= *\<Email\>*

##### Informations optionnelles

 - `ressource_link_description`= *\<Description\>*
 - `user_image`= *\<url\_to\_square_avatar\>*
 - `context_type`= *\<context\_1\>,\<context\_2\>,...*
 - `launch_presentation_locale`= *\<locale_LOCALE\>*
 - `launch_presentation_css_url`= *\<url\_to\_css\>*
 - `tool_consumer_instance_description`= *\<TC_description\>*
 - `tool_consumer_instance_url`= *\<TC\_domain\>*

Le champ suivant est à utiliser uniquement quand l'un des rôle de l'utilisateur est pour **url:lti:role:ims/lis/Mentor**

 - `role_scope_mentor`= *\<Mentor\_role\_id\_1\>,\<Mentor\_role\_id\_2\>,...*

##### Paramètres supplémentaires

Il est possible d'ajouter des paramètres personnalisés.
Le nom du paramètre *POST* doit correspondre au format suivant *"custom\_[a-z0-9]+"*

#### Url de retour

L'url de retour est spécifiée par le champs `launch_presentation_return_url`.

Le TP **peut** ajouter **UNE** des informations suivantes par la méthode *GET* :

 - `lti_errormsg`= *\<Erreur\_pour\_l\_utilisateur\_final\>*
 - `lti_errorlog`= *\<Erreur_log\>*
 - `lti_msg`= *\<Message\_pour\_l\_utilisateur\_final\>*
 - `lti_log`= *\<Message\_log\>*

### Représentation d'un lien basic

```XML
<resource identifier="I_00010_R" type="imsbasiclti_xmlv1p0">
    <file href="I_00001_R/BasicLTI.xml" />
</resource>
```

*I\_00001\_R/BasicLTI.xml*

```XML
<?xml version="1.0" encoding="UTF-8"?>
<cartridge_basiclti_link
    xmlns="http://www.imsglobal.org/xsd/imslsticc_v1p0"
    xmlns:blti="http://www.imsglobal.org/xsd/imsbasiclti_v1p0"
    xmlns:lticm="http://www.imsglobal.org/xsd/imsslticm_v1p0"
    xmlns:lticp="http://www.imsglobal.org/xsd/imsslticp_v1p0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
        http://www.imsglobal.org/xsd/imsslticc_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imsslticc_v1p0.xsd
        http://www.imsglobal.org/xsd/imsbasiclti_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imsbasiclti_v1p0.xsd
        http://www.imsglobal.org/xsd/imslticm_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticm_v1p0.xsd
        http://www.imsglobal.org/xsd/imslticp_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticp_v1p0.xsd
    ">
    ...
</cartridge_basiclti_link>
```

Les paramètres préfixés par *secure_* correspondent à des connexions https (sécurisées).

#### Les balises requises

 - `<blti:launch_url>...</blti:launch_url>` et  
   `<blti:secure_launch_url>...</blti:secure_launch_url>` - URL pour la requête *launch*.
 - `<blti:title>...</blti:title>` - Titre de l'exercice.
 - `<blti:description>...</blti:description>` - Description de l'exercice.
 - `<blti:title>...</blti:title>` - Titre (préféré par rapport au titre du `basic_lti_link`).
 - `<cartridge_bundle identifierref="..." />` - 
 - `<cartridge_icon identifierref="..." />` - 

#### Les balises optionnelles

 - `<blti:icon>...</blti:icon>` et  
   `<blti:secure_icon>...</blti:secure_icon>` - Url de l'icône

#### La section `<lbti:custom>...</lbti:custom>`

Cette section peut contenir plusieurs balises du modèle suivant :

```XML
<parameter key="the-key">the-value</parameter>
```

Ces paramètres doivent être renvoyés au TP lors de la requête *launch*.
Ces paramètres doivent être maintenu lors du processus import/export.

#### La section `<lbti:extensions>...</lbti:extensions>`

Cette section a pour but d'informer le TC des extensions installés sur le TP. Ainsi le TC peut fournir des paramètres supplémentaires.


