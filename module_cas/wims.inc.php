<?php

if (class_exists("wims"))
  return ;

/**
 * This class give us some methods to initialize connection between a server
 * and a server wims.
 *  
 * author: Christophe Lefevre - christophe.lefevre@u-psud.fr
 * date:  16-01-2009
 * update: 31-08-2009 
 *   
 */   
class wims {

  /**
   * xml filename which contains informations about connections to wims's server
   */     
  var $xml_filename=NULL;  
  
  /**
   * variable to store informations (rclass, qclass) for alla available classes
   */     
  var $get_classes=array();

  /**
   * all available classes for a user from a server.
   * this array containst for each classe: rclas, qclass and description   
   */     
  var $available_classes=array();
  
  /**
   *
   */
  var $xml_data=NULL;   
        

  /**
   * Default constructor which check informations about server    
   *
   * @return void   
   *    
   */        
  function __construct() {
    include dirname(__FILE__)."/config.inc.php";
           
    #$this->xml_filename=dirname(__FILE__)."/".$get_classes_file;
  
    # auto check server informations
    if (!$this->check_server_ident()) {
      echo "<br>Unable to get information about WIMS server. Script is halted<br>";
      die();
    }
  }
    
  /**
   * This function will check is server is recognized on wims
   * There is no parameter, we take config file data
   * 
   * @return boolean true if server is recognized 
   *  
   */    
  function check_server_ident() {  
    include dirname(__FILE__)."/config.inc.php";    

    $result=$this->exec_url_to_array($wims_cgi_url."?module=".$module."&code=".$ident_session."&ident=".$ident_server."&passwd=".$password_server."&job=checkident");  
      
    if (strcasecmp($result[0], "OK") == 0)
      return true;
      
    # if we are here, our server haven't be recognized (default result to avoid hacking attempt)    
    echo implode(" ", $result);  
    return false;
  }
  
  /**
   * Call this function to execute a url (with job) and get the return. There is 
   * only one verification if url can't be opened  
   *  
   * @param url string url to execute
   * 
   * @return string string of result  
   */    
  private function exec_url($url) {
    $file=fopen($url, "r");
    $return="";
    
    if ($file) {
      $return=fread($file, 2048);
      fclose($file);
    }
    else
      $return="WARNING: could not open ".$url;    
  
    return $return;
  }
    
  /**
   * this is the same function than exec_url, but, it returns array instead of string
   * 
   * @param url string url to execute
   * 
   * @return array array of result
   *  
   **/     
  private function exec_url_to_array($url) {
    return explode(" ", $this->exec_url($url));
  }
  
  
  /**
   * this function checks if return of wims is right (OK)
   *
   * @param result string result of wims job
   * 
   * @return boolean true if job is ok
   *          
   **/           
  function is_ok($result) {
    $result=explode("\n", $result);
    
    # the first line containst information
    $is_ok=explode(" ", $result[0]);
      
    return (strcmp($is_ok[0], "OK") == 0);
  }
  
  /**
   * This function allows to exec a job with parameters or not. Just give the
   * name of the job, the url is automaticaly build to avoid hacking attempt.   
   * 
   * @param job string name of the job
   * @param param_job string string of job's parameters in url format (don't start by &)
   * 
   * @return string result in a string                  
   *
   */        
  function exec_job($job, $param_job="") {
    include dirname(__FILE__)."/config.inc.php";
  
    return $this->exec_url($wims_cgi_url."?module=".$module."&code=".$ident_session."&ident=".$ident_server."&passwd=".$password_server."&job=".$job."&".$param_job);
  }

  /**
   * this is the same function than exec_job, but, it returns array instead of string
   * 
   * @param job string name of the job
   * @param param_job string string of job's parameters in url format
   * 
   * @return array array of result
   *  
   **/    
  function exec_job_to_array($job, $param_job="") {
    return explode(" ", $this->exec_job($job, $param_job));
  }
  
  
  /**
   * This function returns wims's login from configuration variables
   * You can specify authentication method, if you want to use another method
   * than config file. It can be usefull if wims classes don't use the same
   * login syntax
   * 
   * @param method string name og the authentication method (default: empty => use config file method)
   * 
   * @return login string login to connect to wims class                     
   *    
   */     
  function get_login($method="") {
    # check method name and method filename
    if (empty($method)) {
      include dirname(__FILE__)."/config.inc.php";

      $method=$account_method;
    }    
    
    if (!file_exists(dirname(__FILE__)."/".$method.".inc.php")) {
      echo "<br>WARNING: ".$method." file does not exist. Using default method: authentication<br>";
      $method="authentication";      
    }        
        
    # build object and get wims's login
    $authentication=new $method();               
    
    return $authentication->get_login();  
  }
  
  /**
   * This function will connect user on wims server. Return the result of authuser job
   * 
   * @param qclass string ident of wims class (often a number)
   * @param rclass string name of the class for this server         
   * @param quser string login of the user to authenticate
   *    
   * @return string job's result string
   *             
   */      
  function authuser($qclass, $rclass, $quser) {
    return $this->exec_job("authuser", "qclass=".$qclass."&rclass=".$rclass."&quser=".$quser);
  }    
  
  /**
   * This function will check if user can be authentified by wims server. If it's
   * right, this function will show a link with opened session on wims or
   * will redirect to wims (value of boolean's variable link)      
   *      
   * @param qclass string ident of class on wims server
   * @param rclass string ident of class for this server
   * @param quser string username in wims for current user
   * @param link_text string string to show as a link (default "Connexion")
   * @param link boolean if it's true, show a link else it will automaticaly redirect to wims server (default true)             
   *
   * @return void
   */              
  function user_connect($qclass, $rclass, $quser, $link_text="Connexion", $link=true) {
    include dirname(__FILE__)."/config.inc.php";
  
    $authuser=explode(" ", $this->authuser($qclass, $rclass, $quser));
    
    # check if authentication is right
    if (strcasecmp($authuser[0], "OK") != 0) {
      echo "
      <br>Connexion �chou�e. 
      <br>Votre compte n'est pas reconnu pour la classe s�lectionn�e.
      <br>Vous pouvez acc�der au serveur Wims via l'adresse suivante: 
      <br><a href=\"".$wims_cgi_url."\">".$wims_cgi_url."</a>";
      die();
    }
  
    $authuser=explode("\n", $authuser[1]);
    # check also session id to avoid hacking attempt
    if (strcmp(session_id(), $authuser[0]) != 0) {
      echo "<br>ERROR: session id is not correct. Script halted<br>";
      die();
    }    

    $authuser[1]=str_replace("queryclass=", "", $authuser[1]);
  
    # show just a link or open wims page? by default (link=true), just show a link
    $script="document.write('<a href=\"".$wims_cgi_url."?session=".$authuser[1]."\" target=\"_blank\">".$link_text."</a>');";

    if (!$link)
      $script="document.location='".$wims_cgi_url."?session=".$authuser[1]."';";              
    
    echo "
    <script type=\"text/javascript\">
    <!--
    
    ".$script."
    
    -->
    </script>
    ";  
    echo "<a href=\"".$wims_cgi_url."?session=".$authuser[1]."&+lang=fr&+module=adm%2Fclass%2Fclasses&+type=authparticipant&+class=2008000%2F1%2F101%2F203&+auth_user=iblanc\" target=\"_blank\">".$authuser[1]."</a>";
    
  }
  
  /**
   * this function generate XML file with all available classes from downloaded file
   * it will copy file info getclasses.xml file (check rights of this file)
   * This file is only generated one time by 24 hours.   
   * 
   * @return void      
   *         
   */        
  function get_classes() {
    include dirname(__FILE__)."/config.inc.php";
    
    # if file already exists, check if last update is before yesterday. This
    # is updated only in this case, else this function will do nothing
    if (file_exists(dirname(__FILE__)."/".$get_classes_file)) {
      $last_update=time()-filemtime(dirname(__FILE__)."/".$get_classes_file);
      
      if ($last_update < 60)
        return ;        
    }

    #echo "update in progress";
    
    $source=fopen($get_classes_url.$get_classes_file, "r");
    $dest=fopen(dirname(__FILE__)."/".$get_classes_file, "w"); 
      
    # read and copy data           
    if ($source && $dest)      
      fwrite($dest, fread($source, 4096), 4096);      
    else
      echo "<br>WARNING: unable to read source file and/or to write xml file. Update failed<br>";   
    
    fclose($source);
    fclose($dest);    
  }
  
  

  
  /**
   * This function build an array with each available classes from xml data.
   * type can be string or file. For type=string, xml_data is used
   * 
   * @param type string type of variable (file or string)
   * @param xml_data SimpleXMLElement xml data (only used for string type)            
   * 
   * @return classes_xml's object
   */     
  function list_classes($type="file", $xml_data="") {
    include dirname(__FILE__)."/config.inc.php";    
    
    # only real_time value regenerate text file
    if ((strcmp($get_classes, "real_time") == 0) && (strcmp($type, "file") == 0)) {
      $this->get_classes();
    }    
    
    # read xml data and return result into an array
    $xml=new classes_xml();
    
    # xml data can be a string or a file
    if (strcmp($type, "string") == 0)
      $xml->initialize_data_from_string($ident_server, $xml_data);
    else
      $xml->initialize_data_from_file($ident_server, $this->xml_filename);

    return $xml;    
  }
  
  /**
   * This function returns html select code with available classes for this server
   * It's very usefull if you have more than 1 class 
   * 
   * @param select_name string name of the html select markup
   * @param select_text string default text to show in select markup
   * @param form_id string id of the form, to submit automaticaly on change
   * @param type string type of data (string or file, default file)
   * @param xml_data SimpleXMLElement xml data used for string type      
   * 
   * @return string html select code
   *                          
   */     
  function select_classes($select_name, $select_text, $form_id) {
    $return="
    <select name=\"".$select_name."\" onchange=\"document.getElementById('".$form_id."').submit();\">
      <option value=\"\">".$select_text."</option>";
      
    foreach ($this->classes as $index => $infos)
      $return .="
      <option value=\"".$infos["qclass"]."|".$infos["rclass"]."\">".str_replace("\r\n", "", utf8_decode($infos["description"]))."</option>";

    return $return."
    </select>\n";
  }
  
  
  function select_classes_user_from_job($select_name, $select_text, $form_id, $method="") {
    $this->get_classes_from_job();
    $this->get_classes_user_from_job($method);
        
    return $this->select_classes($select_name, $select_text, $form_id);
  }
  

  function get_classes_user_from_job($method="") {
    include dirname(__FILE__)."/config.inc.php";        
    $request=$wims_cgi_url."?module=".$module."&code=".$ident_session."&ident=".$ident_server."&passwd=".$password_server."&job=getclassesuser&quser=".$this->get_login($method)."&option=description";
    $result=$this->exec_url($request);
    $result=explode("\n", $result);
    
    if (!$this->is_ok($result[0])) {
      echo "ERROR while checking list classes. Script halted";
      debug($result);
      die();  
    }
    
    $this->classes=array();   
    $result=explode(",", $result[1]);

    foreach($result as $index => $qclass ) {
      if (array_key_exists($qclass, $this->get_classes)) {
        $this->classes[]=array(
                    "qclass" => $qclass,
                    "rclass" => $this->get_classes[$qclass][0],
                    "description" => $this->get_classes[$qclass][1] 
                    );                                              
      }        
    }  
  }  

  /**
   * this function will get informations about available classes to current server
   * this is the same purpose that get_classes but this function use wims job   
   * 
   * @return void
   */     
  function get_classes_from_job() {
    include dirname(__FILE__)."/config.inc.php";
    
    $request=$wims_cgi_url."?module=".$module."&code=".$ident_session."&ident=".$ident_server."&passwd=".$password_server."&job=listclasses&option=description";
    $result=$this->exec_url($request);
    $result=explode("\n", $result);   
    
    if (!$this->is_ok($result[0])) {
      echo "ERROR while checking list classes. Script halted";
      debug($result);
      die();  
    }  
        
    $this->build_xml_from_wims($result);
    $this->get_classes=array();  
    
    foreach ($this->xml_data->children() as $class) {   
      if (empty($class->description))
        $class->description=(string)$class->qclass;
         
      $this->get_classes[(string)$class->qclass]=array((string)$class->connection->rclass, (string)$class->description);      
    }
  }
  
  
  function build_xml_from_wims($result) {
    if (!is_array($result))
      $result=explode("\n", $result);         

    unset($result[0]);   
    $this->xml_data=simplexml_load_string(utf8_encode(implode("\n", $result)));

    if ($this->xml_data === false) {      
      echo "Error while creating XML data. Script halted.";
      die();
    }      
  }  
}

?>
