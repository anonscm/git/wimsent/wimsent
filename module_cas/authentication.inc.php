<?php

if (class_exists("authentication"))
  return ;
  
/**  
 * This is a generic class to authenticate an user. This class must not be 
 * call directly... maybe an interface would have been better. 
 * 
 * author: Christophe Lefevre - christophe.lefevre@u-psud.fr
 * date:  16-01-2009
 *   
 **/  
class authentication {

  /**
   * store some common variables
   */     
  var $session_field="";
  var $login_field="";
  var $method="default";

  /**
   * Default contructor, must be called in subclass constructor to initialize 
   * common variables
   * 
   * @return void      
   *    
   */        
  function __construct() {
    include dirname(__FILE__)."/config.inc.php";
  
    $this->method=$account_method;
    $this->session_field=$account_session_field;
    $this->login_field=$account_login_field;    
  }

  /**
   * Default get_login method. Just return account_session_field value
   * 
   * @return string session_field value     
   *
   */        
  function get_login() {
    echo "<br>WARNING: this is default class. Login = account_session_field<br>";    
    
    return $this->get_session_value($this->session_field);
  }

  /**
   * This method try to find SESSION value with field parameter
   * 
   * @param field mixed value to find into SESSION variable
   * 
   * @return mixed value of SESSION field if found, empty if not             
   *     
   */     
  function get_session_value($field) {   
    # if SESSION[$field] exists, return it   
    if (isset($_SESSION[$field]))
      return $_SESSION[$field];

    # check if we need to scan SESSION variable to get right value
    if (is_array($field)) {
      $data=$_SESSION;      
      foreach ($field as $key => $value)
        $data=$data[$value];
    
      return $data;
    }
    
    # not found, not an array => just return empty value
    return ;
  }

}  
