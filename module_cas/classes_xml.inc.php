<?php

if (class_exists("classes_xml"))
  return ;

/**
 * This class will build a array of simple XML objects with informations 
 * about classes. This can be used to generate html code
 * 
 * This class use SimpleXMLElement object, this extension is enabled 
 * by default on PHP5
 * 
 * SimpleXMLElement can't be extended because constuctor is a final method  
 * 
 * author: Christophe Lefevre - christophe.lefevre@u-psud.fr
 * date:  12-02-2009  
 * update: 11-05-2009  
 *
 */    

class classes_xml {
  
  /**
   * name of XML file   
   */
  var $file="";
  
  /**
   * server ident to connect to wims
   */             
  var $ident="";   
  
  /**
   * array of SimpleXMLElement objects
   */        
  var $xml=array();

  /**
   * Default constructor will initialize data
   *
   */        
  function __construct($ident_server, $file="getclasses.xml") {
    $this->ident=$ident_server;
    
    if (file_exists($file))
      $this->file=$file;      
    else
      die("WARNING: ".$file." doesn't exists. Script halted");
      
    $this->build_xml();        
  }

  /**
   * this function build an array with all usefull informations about connection
   * to wims's class   
   *
   * @return void    
   */           
  function build_xml() {
    $xml_tmp=simplexml_load_file($this->file);    
    
    foreach ($xml_tmp->children() as $child) {
      $keep_child=false;        

      if (sizeof($child->connection) == 1) {
        if (strcmp($child->connection->ident, $this->ident) == 0) {
          $child->addChild("rclass", $child->connection->rclass);
          $keep_child=true; 
        }          
      }
      else {
        foreach ($child->connection as $connection) {    
          if (strcmp($connection->ident, $this->ident) == 0) {
            $child->addChild("rclass", $connection->rclass);
            $keep_child=true;
            # only one connection is stored by server
            break;            
          }               
        }      
      }
                                
      if ($keep_child) {
        # remove node to keep only usefull informations
        unset($child->connection);
        # store information under qclass value of class
        $this->xml[(string)$child->qclass]=$child;
      }                                                
    }
  }

}

?>
