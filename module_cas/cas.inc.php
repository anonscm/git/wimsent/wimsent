<?php

if (class_exists("cas"))  
  return ;


/**  
 * This class extends ldap class to get information about user
 * with CAS variable.  
 * 
 * author: Christophe Lefevre - christophe.lefevre@u-psud.fr
 * date:  19-05-2009
 *   
 **/    
class cas extends ldap {

  /**
   * constructor which call parent constructor and set with right value
   * session_field variable   
   * 
   * @return void
   *          
   */     
  function __construct() {
    parent::__construct();
    
    include dirname(__FILE__)."/config.inc.php";      
    $this->session_field=$account_cas_field;        
  }
}

?>  
