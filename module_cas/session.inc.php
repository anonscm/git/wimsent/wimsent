<?php

if (class_exists("session"))  
  return ;


/**  
 * This class extends authentication class to get information about user
 * with SESSION variable.  
 * 
 * author: Christophe Lefevre - christophe.lefevre@u-psud.fr
 * date:  03-02-2009
 *   
 **/    
class session extends authentication {

  /**
   * constructor which call parent constructor
   * 
   * @return void
   *          
   */     
  function __construct() {
    parent::__construct();
  }
  
  
  /**
   * this function returns wims's login. It also checks if variable is set
   *
   * @return string wims's login   
   */            
  function get_login() {
    if (!isset($_SESSION[$this->login_field]))
      echo "WARNING: SESSION['".$this->login_field."'] is not set.";
  
    return $this->get_session_value($this->login_field);  
  }

}

?>  
