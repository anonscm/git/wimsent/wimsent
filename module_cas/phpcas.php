<?php

include_once dirname(__FILE__).'/phpcas/CAS.php';
phpCAS::client(CAS_VERSION_2_0, 'sso.u-psud.fr',443,'/cas');
phpCAS::setNoCasServerValidation();
phpCAS::forceAuthentication();

// at this step, the user has been authenticated by the CAS server
// and the user's login name can be read with phpCAS::getUser().

// logout if desired
if (isset($_REQUEST['logout'])) {
	phpCAS::logout();
}


?>
