<?php

if (class_exists("ldap"))
  return ;
    
  
/** 
 * This class extends authentication class to use LDAP informations 
 *   
 * author: Christophe Lefevre - christophe.lefevre@u-psud.fr
 * date: 16-01-2009 
 *   
 **/    
class ldap extends authentication {

  /**
   * variables about ldap connection
   */     
  var $host="";
  var $port="";
  var $basedn="";
  var $version="";
  
  /**
   * ldap connection     
   */     
  var $connection=false;

  /**
   * field to use to search wims's login
   */
  var $search_field="";        

     
  /**
   * constructor which call parent constructor and initialize some variables
   * 
   * @return void
   *          
   */     
  function __construct() {
    parent::__construct();
    
    include "config.inc.php";
    
    $this->host=$ldap["host"];
    $this->port=$ldap["port"];
    $this->basedn=$ldap["basedn"];
    $this->version=$ldap["version"];    
    
    $this->search_field=$account_method_field;
  }

  /**
   * This function initialize anonymous connection to LDAP server and store 
   * connection into connection class's variable
   * 
   * @return void          
   *
   */        
  function connection() {
    $this->connection=ldap_connect($this->host, $this->port);
    
    if ($this->connection === false) {
      echo "<br>ERROR: Failed to connect to LDAP server. Script halted<br>";
      die();
    }      

    if (!ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, $this->version)) { 
      echo "<br>ERROR: Failed to set protocol version to ".$this->version.". Script halted<br>";
      die();
    }      

    if(!ldap_bind($this->connection)) {
      echo "<br>ERROR: LDAP bind anonymous failed. Script halted<br>";
      die();
    }           
  }

  /**
   * This function search wims login with class variables. It checks also
   * if connection to LDAP is ready   
   * 
   * @return string wim's login   
   *    
   */          
  function get_login() {    
    # check connection and initialize it if it's necessary
    if ($this->connection === false)
      $this->connection();

    # build filter, make a search 
    $filter="(".$this->search_field."=".$this->get_session_value($this->session_field).")";   
    $search=ldap_search($this->connection, $this->basedn, $filter);        
    
    if ($search === false) {
      echo "<br>ERROR: Unable to make a search on LDAP server. Script halted<br>";
      die();
    }    
     
    # and store result into array     
    $result=ldap_get_entries($this->connection, $search);
       
    # check if result is set and contains only one entry
    if ($result === false) {
      echo "<br>ERROR: Unable to read search entries. Script halted<br>";
      die();    
    }  
    
    if ($result["count"] != 1) {
      echo "<br>ERROR: ".$result["count"]." results found instead of 1. Script halted.<br>";
      die();    
    }    
    
    return $result[0][$this->login_field][0];    
  }

}  

?>
