<?php

############## page specialement faite pour la connexion CAS � WIMS

include dirname(__FILE__)."/__autoload.inc.php";

if (strcmp($account_method, "cas") == 0)
  include_once dirname(__FILE__)."/phpcas.php";
else
  die("Impossible d'acc�der au serveur");
  
?>

<html>

<head>
  <title>Connexion au serveur WIMS</title>

  <link rel="stylesheet" href="css/u-psud.css" />

</head>

<body>

<center>

  <div id="ban">
    <div align="center">
    <br /><br />
      <table class="nav" border="0" cellpadding="0" cellspacing="0" height="32" style="width:680px;position:relative;left:0;">
        <tr>
          <td align="left" width="200px"><a href="http://www.u-psud.fr/"><img src="images/logo_ups.gif" alt="Logo u-psud"/></a></td>

          <td width="5" >
            <img src="images/top_navigation_borderleft.gif" height="32" width="5">
          </td>
          <td class="top_navigation" style="background-image:url('images/top_navigation_bkg.gif');background-repeat:repeat-x;height:32px;background-position:center;" >
            <font color="black">          
            Vous �tes reconnu(e) en tant que <b><?php echo phpCAS::getUser(); ?></b>
            </font>
          </td>
          <td width="6">
            <img src="images/top_navigation_borderright.gif" height="32" width="6">
          </td>
        </tr>
      </table>
    </div>
  </div>

  <br>

  <div id="contenant" style="width: 680px;" align="center">
    <img src="images/page_bkg_top_end.gif" alt="" id="bkg_top_end" />
    <h1>Connexion au serveur WIMS <b>http://cluster.calcul.u-psud.fr</b>:</h1>
    

    <form method="post" id="class"> 
<?php



$wims=new wims();

echo $wims->select_classes_user_from_job("wims_class", "Choix de la classe wims", "class");

?>

    </form>

<?php
if (isset($_POST) && !empty($_POST['wims_class'])) {
  $class=explode("|", $_POST['wims_class']);  
  $wims->user_connect($class[0], $class[1], $wims->get_login(), false, false);   
}
else {
  echo "
    <br>Acc�s anonyme au serveur:
    <br><a href=\"".$wims_url_simple."/wims/wims.cgi?lang=fr\">".$wims_url_simple."</a>
    <br>";
     
}  
?>

<br><br><br>
    <h1>Connexion au serveur WIMS de l'IUT de Cachan<br><b>http://wims.iut-cachan.u-psud.fr</b>:</h1>

    Acc�s anonyme au serveur:
    <br><a href="http://wims.iut-cachan.u-psud.fr/wims/wims.cgi">http://wims.iut-cachan.u-psud.fr/wims/wims.cgi</a>
    <br>


</div>
</center>

