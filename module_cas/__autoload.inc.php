<?php

include dirname(__FILE__)."/config.inc.php";

/**
 * autoload function. this is the default function to include class files.
 * 
 * @param class string name of the class  
 *
 * @return void
 *  
 * author: Christophe Lefevre - christophe.lefevre@u-psud.fr
 * date: 16-01-2009 
 *  
 */ 
function __autoload($class) {
  $file=dirname(__FILE__)."/".$class.".inc.php";

  if (file_exists($file))
    include_once $file;
}



function debug($var) {
  echo "<div align=\"left\">";
  echo "<pre style=\"background-color: #FFFFCC\" align=\"left\">";
  $result=print_r($var, true);
  echo htmlentities($result);
  echo "</pre></div>";
}

function nfo($text) {
  echo "<pre style=\"background-color: #99CCFF\">".$text."</pre>";
}


?>
