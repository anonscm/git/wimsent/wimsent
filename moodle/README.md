Purpose
=======

Enable Moodle adminstrators to let teachers add Wims resources
for their students, with grading features.
