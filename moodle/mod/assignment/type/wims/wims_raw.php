<?php  // $Id: wims_raw.php,v 1.1.1.1 2009-04-04 14:19:23 georgesk Exp $
/**
* @author Georges Khaznadar
* @license http://www.gnu.org/copyleft/gpl.html GNU Public License
* @package wims
*
* The class job_raw implements the protocol described at
* http://wims.unice.fr/wims/?module=adm/raw&job=help
*
* initialisation: job_raw($server,$service,$servicepass)
* parameters:
* * $server is the IP address of the Wims server
* * There must be a file wimshome/log/classes/.connections/$service
* * with a line defining: 'ident_password=$servicepass'
*
* protocol description: help()
* queries the last version of the help page from $this->server.
*
*/

class job_raw{
  var $server;
  var $root;
  var $service;
  var $servicepass;
  var $debug;
  var $data;
  var $qclass;
  var $ok;
  function job_raw($server,$root,$service,$servicepass){
    $this->server=$server;
    $this->root=$root;
    $this->service=$service;
    $this->servicepass=$servicepass;
    $this->qclass="";
    $this->debug=0;
    $this->ok=0;
    $this->code="";
  }
  function wimslogin($login){
    // clean some chars which are not allowed by wims
    return str_replace(".","0",$login);
  }
  function url($which,$parms=""){
    $this->code=random_string(3);
    $url="https://".$this->server."/".$this->root.
      "/wims.cgi?module=adm/raw&job=".$which.
      "&code=".$this->code.
      "&ident=".$this->service.
      "&passwd=".$this->servicepass;
    if (strlen($parms)>0) $url.= "&".$parms;
    if ($this->debug > 0) print(" parms = $parms<br /> url = $url<br />");

  return $url;
  }
  function job($which,$parms=""){
    $url=$this->url($which,$parms);
    if ($this->debug > 0) print("<br />url = ".$url."<br />");
    $f=fopen($url,"r");
    if ($f){
      $this->data=fread($f,2048);
      fclose($f);
    } else {
      $this->data="Error: could not open $url";
    }
    $e=explode("\n",$this->data);
 //   $this->ok=($e[0]=="OK ".$this->code);
	
if($e[0]=="OK ".$this->code){
	
   $this->ok=1;} else{
	$this->ok=0;
}
	


    if ($this->debug > 0) print("<br />ok = ".$this->ok."<br />");
    return $e;
  }

  function help(){
    return $this->job("help");
  }
  function checkident(){
    return $this->job("checkident");
  }
  function checkclass($qcl,$rcl){
    $this->qclass=$qcl;
    $parms="qclass=".urlencode($qcl)."&rclass=".urlencode($rcl);
    return $this->job("checkclass",$parms);
  }
  function getclass($qcl,$rcl){
    $this->qclass=$qcl;
    $parms="qclass=".urlencode($qcl)."&rclass=".urlencode($rcl);
    return $this->job("getclass",$parms);
  }
  function addclass($wc,$rcl){
    // The queried class will always be a new class number, choosen randomly
    // Warning: the random value might collide with an already existing class
    // TODO: make this function safer !
    $qcl=rand(1000000,9999999);
    $parms="data1=".$wc->data1().
      "&data2=".$wc->data2().
      "&qclass=".urlencode($qcl)."&rclass=".urlencode($rcl);
    return $this->job("addclass",$parms);
  }
  function authuser($qcl,$rcl,$login){
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl)."&quser=".$this->wimslogin($login);
    return $this->job("authuser",$parms);
  }
  function get_class($qcl,$rcl){ //getclass is a forbidden name in PHP?
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    return $this->job("getclass",$parms);
  }
  function del_class($qcl,$rcl){ //delclass may be forbidden too?
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    return $this->job("delclass",$parms);
  }
  function adduser($qcl,$rcl,$firstname,$lastname,$login){
    $password=random_string(8); // this password is not useful
    $data1=urlencode("firstname=".$firstname.
		     "\nlastname=".$lastname.
		     "\npassword=".$password."\n");
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    $parms.="&data1=".$data1."&quser=".$this->wimslogin($login);
    return $this->job("adduser",$parms);
  }
  function deluser($qcl,$rcl,$login){
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    $parms.="&quser=".$this->wimslogin($login);
    return $this->job("deluser",$parms);
  }
  function getuser($qcl,$rcl,$login){
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    $parms.="&quser=".$this->wimslogin($login);
    return $this->job("getuser",$parms);    
  }
  function getscore($qcl,$rcl,$login){
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    $parms.="&quser=".$this->wimslogin($login);
    return $this->job("getscore",$parms);    
  }
  function getsheet($qcl,$rcl,$login,$sheet,$format="mean"){
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    $parms.="&quser=".$this->wimslogin($login)."&qsheet=".$sheet."&format=".$format;
    return $this->job("getsheet",$parms);    
  }
  function addsheet($qcl,$rcl,$contents="",$sheetmode="0",$title="",$description="",$expiration=""){
    $contents=str_replace("\n",";",$contents);
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    $data1="";
    if ($title!="")       $data1.=urlencode("title=$title\n");
    if ($description!="") $data1.=urlencode("description=$description\n");
    if ($expiration!="")  $data1.=urlencode("expiration=$expiratiion\n");
    if ($contents!="")    $data1.=urlencode("contents=$contents\n");
    if ($sheetmode!="0")  $data1.=urlencode("sheetmode=$sheetmode\n");
    $parms.="&data1=$data1";
    return $this->job("addsheet",$parms);    
  }
  function listsheets($qcl,$rcl){
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    return $this->job("listsheets",$parms);    
  }
  function getcsv($qcl,$rcl,$option=""){
    // this primitive does not reply "OK" at the first line, since it is
    // designed to output a valid csv file.
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    $parms.="&option=".$option;
    $url=$this->url("getcsv",$parms);
    $f=fopen($url,"r");
    if ($f){
      $this->data=fread($f,204800);
      fclose($f);
      $this->ok=1;
    } else {
      $this->data="Error: could not open $url";
      $this->ok=0;
    }
    return explode("\n",$this->data);
  }
  function getclassfile($qcl,$rcl,$option=""){
    $parms="qclass=".$qcl."&rclass=".urlencode($rcl);
    $parms.="&option=".$option;
    return $this->job("getclassfile",$parms);    
  }
}

?>
