<?php
$string['typewims'] = 'Wims';
$string['type_exe'] = 'Exercice Wims';

$string['type_exe_help'] = 'Choix du type de devoir';

$string['add_this_sheet'] = 'Importer cette feuille de travail';
$string['adminclass'] = 'Administrer cette mini-classe Wims';
$string['adminsheet'] = 'Gérer les contenus dans cette mini-classe Wims';
$string['already_imported'] = 'X';
$string['apply'] = 'Aller dans la mini-classe Wims';
$string['automatic_score'] = 'Commentaire automatique de Wims';
$string['closeandupdate'] = 'Fermer cette fenêtre et mettre à jour dans Moodle';
$string['consider_imports'] = 'Il n\'y a pas en core de feuille de travail dans cette mini-classe.<br>Veuillez importer quelques feuilles de travail précédemment publiées.<br><b>Voir ci-dessous</b>';
$string['create_an_exam'] = 'Il n\'y a pas encore de session d\'examen. Veuillez gérer le contenu de la mini-classe Wims.';
$string['defaultexercise'] = 'Feuille de travail ordinaire';
$string['exam'] = 'Session d\'examen';
$string['exam_meanscore'] = 'Note moyenne de l\'examen :';
$string['examFeedback'] = 'Liste de sessions d\'examen';
$string['getcsv'] = 'fichier CSV';
$string['getexamsource'] = 'Source';
$string['getsheetsource'] = 'Source';
$string['hello_designer'] = 'Type d\'exercice';
$string['import_selected_sheets'] = 'Importer les feuilles de travail sélectionnées';
$string['importableSheets'] = 'Liste des feuilles de travail précédemment publiées (pour importation)';
$string['listofsheets'] = 'Feuilles de travail disponibles dans cette mini-classe Wims';
$string['makeasheet'] = 'Il n\'y a pas encore de feuille de travail. Veuillez utiliser le bouton ci-dessous et créer au moins une feuille de travail.';
$string['maybewimsbusy'] = 'De temps en temps Wims reste occupé pendant un moment, par exemple pour une maintenance journalière. Vous pouvez réessayer de créer la mini-classe Wims un peu plus tard.';
$string['meansheet'] = 'Note moyenne :';
$string['no_exam_yet'] = 'Aucun examen n\'est encore publié. Revenez plus tard.';
$string['nosheetsyet'] = 'Aucune feuille d\'exercices n\'a encore été publiée. Revenez plus tard.';
$string['retry'] = 'Réessayer';
$string['score_is'] = 'note&nbsp;:&nbsp;';
$string['sessions_done'] = 'sessions ont été terminées.';
$string['sorrynoauthuser'] = 'Désolé, l\'authentification a échoué';
$string['sorrynothing'] = 'Désolé, il n\'y a pas ecore de contenu pour ce devoir';
$string['sorrynotopen'] = 'Désolé, vous ne pouvez pas entrer parce que ce devoir n\'est pas encore ouvert.';
$string['user_data'] = 'Données utilisateur';


?>
