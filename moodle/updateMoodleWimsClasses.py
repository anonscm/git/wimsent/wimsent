# -*- coding: utf-8 -*-
u"""
Utilitaire de mise à jour des activités "classe WIMS" de Moodle.

Permet d'effectuer des opération de maintenances sur l'ensemble
 des classes WIMS présentes dans les cours Moodle.
 Par exemple ici :
    * on met à jour la date d'expiration de la classe
     (pour éviter une éventuelle incohérence (classe expirée sur WIMS mais activité toujours présente dans Moodle))
    * on met à jour les dates d'expiration des feuilles
    * on purge les activités apprenants (optionel)
"""
import sys
import json
import datetime

# SQL Alchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, aliased
from sqlalchemy.pool import NullPool

# URL
import urllib2


class updateMoodle:
    u"""Définition d'un objet pour la mise à jour de Moodle."""

    def __init__(self, site):
        u"""SCRIPT : Initialisation de la classe, chargement des paramètres de connexions."""
        # Parametres à personnaliser
        motdepasse = "VOTRE_MDP_WIMS"
        nouvelle_date = "20200930" # pour le moment, la nouvelle date n'est pas prise en compte : WIMS repousse automatiquement d'un an.
        self.module_id = "22" # id du module "WIMS" dans Moodle
        self.offset_wims = 500000 # départ des numéros de classes WIMS créés par le plugin
        url_wims = "https://lms-wims.unice.fr/wims/?module=adm/raw&ident=moodlejsonhttps&passwd=%s&code=1234&rclass=NOUVELLECLASSE&qclass=CLASS_ID" % (motdepasse)

        self.url_changedates="%s&job=changedates&data1=%s" % (url_wims, nouvelle_date)
        self.url_cleanclass="%s&job=cleanclass" % (url_wims)

        # Load parameters
        src_parameters_dict = json.loads(open("/opt/exportMoodle/config.json").read())

        # Moodle BDD parameters
        self.moodle_db_user = src_parameters_dict[site]["moodle_db_user"]
        self.moodle_db_password = src_parameters_dict[site]["moodle_db_password"]
        self.moodle_db_fqdn = src_parameters_dict[site]["moodle_db_fqdn"]
        self.moodle_db_port = src_parameters_dict[site]["moodle_db_port"]
        self.moodle_db_name = src_parameters_dict[site]["moodle_db_name"]
        self.moodle_db_type = src_parameters_dict[site]["moodle_db_type"]
        self.engineDB = None
        self.session_db = None


    """ Début des fonctions BDD de Moodle """
    def connectMoodle(self):
        u"""BDD :connexion a la base de donnée d'une instance de Moodle."""
        print "connectMoodle"
        if self.moodle_db_type == "mysql":
            url_connexion_mdl = "mysql+mysqldb://%s:%s@%s:%s/%s" % (self.moodle_db_user, self.moodle_db_password, self.moodle_db_fqdn, self.moodle_db_port, self.moodle_db_name)
        elif self.moodle_db_type == "postgresql":
            url_connexion_mdl = "postgresql+psycopg2://%s:%s@%s:%s/%s" % (self.moodle_db_user, self.moodle_db_password, self.moodle_db_fqdn, self.moodle_db_port, self.moodle_db_name)

        SessionDB = sessionmaker()
        self.engineDB = create_engine(url_connexion_mdl, echo=False, poolclass=NullPool)
        SessionDB.configure(bind=self.engineDB)
        self.session_db = SessionDB()
        print "----- CONNEXION MOODLE OK -----"

    def disconnectMoodle(self):
        u"""BDD : déconnexion de la base de données de l'instance de Moodle."""
        print "disconnectMoodle"
        self.session_db.close()
        self.engineDB.dispose()

    def getCourseModuleWims(self):
        u"""BDD : retourne la liste des ID des activités Wims des cours Moodle."""
        print "getCourseModuleWims"
        return self.session_db.execute("SELECT id \
                                        FROM mdl_course_modules \
                                        WHERE module=%s \
                                        ORDER BY id DESC" % self.module_id)

    """ Début des fonctions Script """
    def updateMoodleWimsClasses(self):
        u"""SCRIPT : modifie la date d'expiration d'une classe Wims créé par Moodle, ainsi que de toutes les activités."""
        print "updateMoodleWimsClasses"
        for module_wims in self.getCourseModuleWims().fetchall():
            print module_wims[0]
            url_wims = self.url_changedates.replace("NOUVELLECLASSE", "moodle_%s" % str(module_wims[0]))
            url_wims = url_wims.replace("CLASS_ID", "%s" % str(module_wims[0] + self.offset_wims))
            print url_wims
            req = urllib2.Request(url_wims, headers={"User-Agent": "Moodle"})
            print urllib2.urlopen(req).read()

    def cleanMoodleWimsClasses(self):
        u"""SCRIPT : supprime les activités étudiantes d'une classe Wims créé par Moodle."""
        print "cleanMoodleWimsClasses"
        for module_wims in self.getCourseModuleWims().fetchall():
            print module_wims[0]

            url_wims = self.url_cleanclass.replace("NOUVELLECLASSE", "moodle_%s" % str(module_wims[0]))
            url_wims = url_wims.replace("CLASS_ID", "%s" % str(module_wims[0] + self.offset_wims))
            print url_wims
            req = urllib2.Request(url_wims, headers={"User-Agent": "Moodle"})
            print urllib2.urlopen(req).read()


def main(argv):
    """Fonction main pour execution depuis le SHELL."""
    if argv[-1] == "LOG":
        sys.stdout = open("/opt/exportMoodle/Log/updateMoodleWimsClasses%s.log" % (argv[0]), "w+")

    script_start = datetime.datetime.now().strftime("%d-%m-%y %H:%M:%S")
    print "DEB : %s" % script_start

    argv_len = len(argv)
    if  argv_len < 1:
        print "Paramètres attendus 1 minimum, paramètre(s) donné(s) : %s" % argv_len
        return "END"

    update = updateMoodle(argv[0])

    update.connectMoodle()

    update.updateMoodleWimsClasses()

    # Retirer le commentaire si vous souhaitez également supprimer toutes les activités étudiantes des classes
    # update.cleanMoodleWimsClasses()

    update.disconnectMoodle()

    print "DEB : %s" % script_start
    print "FIN : %s" % datetime.datetime.now().strftime("%d-%m-%y %H:%M:%S")

    print "Done"

""" Lancement de la fonction main avec passage des paramètres du SHELL """
if __name__ == "__main__":
    main(sys.argv[1:])
