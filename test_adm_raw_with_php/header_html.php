
<meta charset="UTF-8">

<!-- If you delete this meta tag, World War Z will become a reality -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Foundation CSS -->
<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.css">
<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.css">

<script  src="//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/vendor/modernizr.js"></script>

<!-- Json Syntax Highlighting -->
<link rel="stylesheet" href="json_highlight.css">
<script src="json_highlight.js"></script>

<!-- Font awesome CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<link rel="shortcut icon" type="image/x-icon" href="wims-black-100.png" />
<link rel="stylesheet" type="text/css" href="style.css"/>
