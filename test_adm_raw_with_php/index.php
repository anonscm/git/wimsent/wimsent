<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Interconnexions WIMS - ENT</title>

  <?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    include("header_html.php");
    include("config.php");

    include("datas/class_datas.php");
    include("datas/activities.php");

    $subclass = $qclass.$sous_classe;

    $url = $protocol."://".$serveur."/wims/wims.cgi?module=adm/raw&ident=".$local_ident."&passwd=".$local_pass."&code=".$rand_code;
    ?>

  <style>
    iframe{
      width:100%;
      height:100%;
      min-height:800px;
      border:none;
    }
    #rightFrame{
      position: fixed;
      max-width: 48%;
      max-height: 95%;
    }
  </style>
</head>
<body>
  <div class="frameset">
    <div class="frame column medium-6" id="leftFrame">
        <?php include("test_suite1.php"); ?>
    </div>
    <div class="frame column medium-6">
      <pre id="rightFrame"></pre>
      <div style="margin-top:25%;" class="panel radius callout">
        <p>Vous verrez apparaitre ci-dessus les réponses du serveur WIMS</p>
      </div>
    </div>
  </div>

<fieldset>
  <legend>Tests PHP:</legend>
  <?php
    echo "<p>voici un \R : ".urlencode("\r")."</p>";
    //-->reponse : "%0D"
    echo "<p>voici un \N : ".urlencode("\n")."</p>";
    //-->reponse : "%0A"
    foreach ($_GET as $indice => $valeur) {
        echo htmlentities($indice.' = '.$valeur).'<br />';
    }
    ?>
</fieldset>


<?php include("footer_html.php"); ?>

</body>
</html>
