

/*var windowWidth = 800;
var windowHeight = 800;
var windowLeft = parseInt((screen.availWidth/2) - (windowWidth/2));
var windowTop = parseInt((screen.availHeight/2) - (windowHeight/2));
var windowSize = "width=" + windowWidth + ",height=" + windowHeight + ",left=" + windowLeft + ",top=" + windowTop + ",scrollbars=yes";*/

$(document).ready(function(){
  /*$('.bouton').click(function (event){

      var url = $(this).attr("href");
      var windowName = $(this).attr("target");

      window.open(url, windowName, windowSize);

      event.preventDefault();

  });*/


  $(".button:not(.download)").click(function(event) {
    event.preventDefault();
    rightframe_jq = $("#rightFrame")
    rightframe_jq.css("border-color", "orange");
    rightframe = document.getElementById('rightFrame');
    rightframe.innerHTML = "Chargement...";
    var xhr = new XMLHttpRequest();
    xhr.open('GET', $(this).attr('href'));
    xhr.onload = function() {
      if (xhr.status === 200) {
        rightframe.innerHTML = json_syntaxHighlight(xhr.responseText);
        rightframe_jq.css("border-color", "green");
      }
      else {
        alert('Request failed.  Returned status of ' + xhr.status);
        rightframe_jq.css("border-color", "red");
      }
    };
    xhr.send();
  });
});
