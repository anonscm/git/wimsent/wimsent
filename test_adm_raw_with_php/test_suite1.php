
<h1>
  <img src="wims-black-100.png" alt="logo Wims" class="image" />
  Bienvenue sur le test d'interactions wims &hArr; php
</h1>

<h2>Suite de tests n°1</h2>
<p>
  Cette page ne fonctionne qu'en appelant le serveur <b><?php echo($serveur); ?></b>
  depuis l'ordinateur client <b><?php echo($local_ident); ?></b>.
</p>
<p>
  Vous pouvez modifier ces paramètres dans le fichier "config.php"<br/>
  Vous trouverez plus d'infos sur le <a href="http://wimsedu.info/wimsedu/wiki/doku.php?id=ent:a">wiki de Wims Edu</a>.
</p>

<h3 align = "center">Procédons par étapes.</h3>
  <fieldset>
    <legend>Tests Préliminaires</legend>
    <ol class="liste_tests">
    <li>
      On VERIFIE la CONNEXION (mode GET) :
      <a class="button small radius"
         href="<?php echo($url); ?>&amp;job=checkident"
         target="rightFrame"><i class="fa fa-key"></i> Connecter</a>
    </li>
    <li>
      <form method="post" action="<?php echo($protocol."://".$serveur."/wims/wims.cgi"); ?>"
            target="rightFrame">
      On VERIFIE la CONNEXION (mode POST):
        <input type="hidden" name="job"    value="checkident" />
        <input type="hidden" name="module" value="adm/raw" />
        <input type="hidden" name="code"   value="<?php echo($rand_code);?>" />
        <input type="hidden" name="passwd" value="<?php echo($local_pass);?>" />
        <input type="hidden" name="ident"  value="<?php echo($local_ident);?>"/>
        <button type="submit" class="button small radius download">
            <i class="fa fa-key"></i> Connecter
        </button>
      </form>
    </li>
  </ol>
  </fieldset>
  <fieldset>
    <legend>Méthodes sur les CLASSES</legend>
    <ol class="liste_tests" start="3">
    <li class="classe">
      On VERIFIE qu'il existe une <strong>CLASSE</strong>
      <b><?php echo($qclass); ?></b>
      sur wims liée à notre classe locale
      <a class="button small radius" href="<?php echo($url); ?>&amp;job=checkclass&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
         target="rightFrame"><i class="fa fa-terminal"></i> Tester</a>
    </li>
    <li class="classe">
      On demande les MODIFICATIONS effectuées sur la <strong>CLASSE</strong>
      <b><?php echo($qclass); ?></b>
      depuis la veille
      <a class="button small radius" href="<?php echo($url); ?>&amp;job=getclassmodif&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-terminal"></i> Tester</a>
    </li>
    <li class="classe">
      On demande les INFOS concernant la
      <strong>CLASSE <?php echo($qclass); ?></strong>
      sur Wims liée à notre classe locale
      <a class="button small radius" href="<?php echo($url); ?>&amp;job=getclass&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;option=all_even_hidden" target="rightFrame"><i class="fa fa-info-circle"></i> infos</a>
    </li>

    <li class="classe">
    <li class="participant">
        AJOUT du <strong>PARTICIPANT</strong>
        "<b><?php echo($quser); ?></b>" sur la sous-classe Wims n°
        <b><?php echo($subclass); ?></b>
        <a class="button small radius success" href="<?php echo($url); ?>&amp;job=adduser&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($subclass); ?>&amp;quser=<?php echo($quser); ?>&amp;data1=<?php echo($data_user); ?>"
           target="rightFrame"><i class="fa fa-plus"></i> Créer</a>
    </li>
      AJOUT d'une nouvelle <strong>CLASSE</strong>
      sur Wims n°<b><?php echo($qclass); ?></b>
      <a class="button small radius success" href="<?php echo($url); ?>&amp;job=addclass&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;data1=<?php echo($data_1); ?>&amp;data2=<?php echo($data_2); ?>"
         target="rightFrame"><i class="fa fa-plus"></i> Créer</a>
         <div><em>nb : si c'est un groupement de classe, une sous-classe sera créée.</em></div>
    </li>
    <li class="classe">
      AJOUT d'une nouvelle <strong>CLASSE</strong>
      sur WIMS (numéro choisi par WIMS)
      <a class="button small radius success" href="<?php echo($url); ?>&amp;job=addclass&amp;rclass=<?php echo($rclass); ?>&amp;data1=<?php echo($data_1); ?>&amp;data2=<?php echo($data_2); ?>"
           target="rightFrame"><i class="fa fa-plus"></i> Créer</a>
    </li>

    <li class="classe">
      AJOUT d'une nouvelle <strong>CLASSE</strong> avec paramètres erronés
      <a class="button small radius success" href="<?php echo($url); ?>&amp;job=addclass&amp;rclass=<?php echo($rclass); ?>&amp;data1=<?php echo($data_err1); ?>&amp;data2=<?php echo($data_err2); ?>"
           target="rightFrame"><i class="fa fa-plus"></i> Créer</a>
    </li>

    <?php $newDesc="c'est une super classe" ?>

    <li class="classe">
      MODIFICATION du titre en
      <b><?php echo($newDesc); ?></b>
      de la <strong>classe</strong>
      Wims n°<b><?php echo($qclass); ?></b>
      <a class="button small radius" href="<?php echo($url); ?>&amp;job=modclass&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;data1=description=<?php echo($newDesc); ?>"
         target="rightFrame"><i class="fa fa-pencil"></i> Modifier</a>
    </li>

    <li class="classe">
      AJOUT de la CONNEXION à un autre serveur pour la
      <strong>classe n°<?php echo($qclass); ?></strong>
      <a class="button small radius"
         href="<?php echo($url); ?>&amp;job=modclass&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;data1=connections=nouveauserveur/classe_lie"
         target="rightFrame"><i class="fa fa-pencil"></i> Modifier
      </a>
    </li>

    <li class="classe">
      Duplique la <strong>classe n°<?php echo($qclass); ?></strong>
      <a class="button small radius"
         href="<?php echo($url); ?>&amp;job=copyclass&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>"
         target="rightFrame"><i class="fa fa-copy"></i> Copier
      </a>
    </li>
    </ol>
    </fieldset>
    <fieldset>
      <legend>UTILISATEURS :</legend>
      <ol class="liste_tests" start="10">
      <li class="participant">
        AJOUT d'un nouveau <strong>PARTICIPANT</strong>
        "<b><?php echo($quser); ?></b>" sur la classe sur Wims n°
        <b><?php echo($qclass); ?></b>
        <a class="button small radius success" href="<?php echo($url); ?>&amp;job=adduser&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;quser=<?php echo($quser); ?>&amp;data1=<?php echo($data_user); ?>"
           target="rightFrame"><i class="fa fa-plus"></i> Créer</a>
      </li>
      <li class="participant">
          AJOUT du <strong>PARTICIPANT</strong>
          "<b><?php echo($quser); ?></b>" sur la <strong>sous-classe</strong> Wims n°
          <b><?php echo($subclass); ?></b>
          <a class="button small radius success" href="<?php echo($url); ?>&amp;job=adduser&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($subclass); ?>&amp;quser=<?php echo($quser); ?>&amp;data1=<?php echo($data_user); ?>"
             target="rightFrame"><i class="fa fa-plus"></i> Ajouter</a>
      </li>
      <li class="participant">
        On VERIFIE que le <strong>PARTICIPANT</strong>
        "<b><?php echo($quser); ?></b>" existe dans la classe
        <b><?php echo($qclass); ?></b>
        sur Wims liée à notre classe locale
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=checkuser&amp;quser=<?php echo($quser); ?>&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-info-circle"></i> infos</a>
      </li>
      <li class="participant">
        On demande les INFOS concernant le <strong>PARTICIPANT</strong>
        "<b><?php echo($quser); ?></b>" de la classe
        <b><?php echo($qclass); ?></b>
        sur Wims liée à notre classe locale
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=getuser&amp;quser=<?php echo($quser); ?>&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-info-circle"></i> infos</a>
      </li>
      <li class="participant">
        MODIFICATION du <strong>PARTICIPANT</strong>
        "<b><?php echo($quser); ?></b>" sur la classe sur Wims n°
        <b><?php echo($qclass); ?></b>
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=moduser&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;quser=<?php echo($quser); ?>&amp;data1=<?php echo($data_user); ?>"
           target="rightFrame"><i class="fa fa-pencil"></i> Modifier</a>
      </li>
      <li class="participant">
        SUPPRESSION du <strong>Participant</strong>
        "<b><?php echo($quser); ?></b>" sur la classe Wims n°<b><?php echo($qclass); ?></b>
        <a class="button small radius alert" href="<?php echo($url); ?>&amp;job=deluser&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;quser=<?php echo($quser); ?>"
           target="rightFrame"><i class="fa fa-trash"></i> Supprimer</a>
      </li>
      <li class="participant">
        RETROUVER le <strong>PARTICIPANT</strong>
        "<b><?php echo($quser); ?></b>" supprimé de la classe sur Wims n°<b><?php echo($qclass); ?></b>
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=recuser&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;quser=<?php echo($quser); ?>"
           target="rightFrame"><i class="fa fa-terminal"></i> REMETTRE</a>
      </li>
      <li class="participant">
        SUPPRESSION DEFINITIVE du <strong>Participant</strong>
        "<b><?php echo($quser); ?></b>" de la corbeille de la classe Wims n°<b><?php echo($qclass); ?></b>
        <a class="button small radius alert" href="<?php echo($url); ?>&amp;job=deluser&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;quser=<?php echo($quser); ?>&amp;option=del_from_trash"
           target="rightFrame"><i class="fa fa-trash"></i> Supprimer</a>
      </li>
      <li class="participant">
        AUTHENTIFIER le <strong>PARTICIPANT</strong>
        "<b><?php echo($quser); ?></b>" dans sa classe sur Wims n°<b><?php echo($qclass); ?></b>
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=authuser&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;quser=<?php echo($quser); ?>&amp;data1=<?php echo($_SERVER['REMOTE_ADDR'])?>"
           target="rightFrame"><i class="fa fa-key"></i> Connecter</a>
      </li>
      <li class="participant">
        AUTHENTIFIER le <strong>PARTICIPANT</strong>
        "<b><?php echo($quser); ?></b>" dans sa classe sur Wims n°<b><?php echo($qclass); ?></b> (option lightpopup)
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=authuser&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;quser=<?php echo($quser); ?>&amp;data1=<?php echo($_SERVER['REMOTE_ADDR'])?>&amp;option=lightpopup"
           target="rightFrame"><i class="fa fa-key"></i> Connecter</a>
      </li>
      <li>
        On demande les NOTES du PARTICIPANT "<b><?php echo($quser); ?></b>" de la CLASSE
        <b><?php echo($qclass); ?></b> :
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=getscore&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;quser=<?php echo($quser); ?>"
           target="rightFrame"><i class="fa fa-eye"></i> Afficher</a>
      </li>
      <li>
        On demande les NOTES du PARTICIPANT "<b><?php echo($quser); ?></b>" de la CLASSE
        <b><?php echo($qclass); ?></b> pour la FEUILLE
        <b><?php echo($qsheet); ?> uniquement</b> :
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=getscore&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;quser=<?php echo($quser); ?>&amp;qsheet=<?php echo($qsheet); ?>"
           target="rightFrame"><i class="fa fa-eye"></i> Afficher</a>
      </li>
      <li>
        AUTHENTIFIER le <strong>SUPERVISEUR</strong>
        dans sa classe sur Wims n°<b><?php echo($qclass); ?></b>
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=authuser&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;quser=supervisor" target="rightFrame"><i class="fa fa-key"></i> Connecter</a>
      </li>
    </ol>
    </fieldset>

    <fieldset>
      <legend>FEUILLES ET EXAMENS :</legend>
      <ol class="liste_tests" start="20">
      <li class="feuille">
        AJOUT d'une nouvelle <strong>FEUILLE</strong>
        à la classe Wims n°
        <b><?php echo ($qclass); ?></b>
        <a class="button small radius success" href="<?php echo($url); ?>&amp;job=addsheet&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;data1=<?php echo($data_sheet_1); ?>"
           target="rightFrame"><i class="fa fa-plus"></i> Créer</a>
      </li>

      <li class="examen">
        AJOUT d'un nouvel <strong>EXAMEN</strong>
        à la classe Wims n°
        <b>
          <?php echo ($qclass); ?></b>
        <a class="button small radius success" href="<?php echo($url); ?>&amp;job=addexam&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;data1=<?php echo($data_exam); ?>"
           target="rightFrame"><i class="fa fa-plus"></i> Créer</a>
      </li>

      <li class="feuille">
        On VERIFIE qu'il existe une <strong>FEUILLE</strong>
        <b>
          <?php echo ($qsheet); ?></b>
        de la classe <b>
          <?php echo ($qclass); ?></b>
        :
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=checksheet&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qsheet=<?php echo($qsheet); ?>"
         target="rightFrame"><i class="fa fa-terminal"></i> Tester</a>
      </li>

      <li class="examen">
        On VERIFIE qu'il existe un
        <strong>EXAMEN</strong>
        <b>
          <?php echo ($qsheet); ?></b>
        de la classe <b>
          <?php echo ($qclass); ?></b>
        :
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=checkexam&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qexam=<?php echo($qsheet); ?>"
         target="rightFrame"><i class="fa fa-terminal"></i> Tester</a>
      </li>

      <li class="feuille">
        On demande les INFOS concernant la
        <strong>FEUILLE</strong>
        <b>
          <?php echo ($qsheet); ?></b>
        de la classe <b>
          <?php echo ($qclass); ?></b>
        :
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=getsheet&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qsheet=<?php echo($qsheet); ?>"
         target="rightFrame"><i class="fa fa-info-circle"></i> infos</a>
      </li>

      <li class="examen">
        On demande les INFOS concernant l'
        <strong>EXAMEN</strong>
        <b>
          <?php echo ($qsheet); ?></b>
        de la classe <b>
          <?php echo ($qclass); ?></b>
        :
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=getexam&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qexam=<?php echo($qsheet); ?>"
         target="rightFrame"><i class="fa fa-info-circle"></i> infos</a>
      </li>
      </ol>
    </fieldset>

    <fieldset>
      <legend>EXERCICES :</legend>
      <ol class="liste_tests" start="26">
        <li>
          AJOUT d'un
          <strong>EXERCICE</strong>
          existant (<?php echo($exo_module); ?>) à la
          <strong>feuille</strong>
          <b><?php echo ($qsheet); ?></b>
          de la classe WIMS n°<b><?php echo ($qclass); ?></b>
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=putexo&amp;qsheet=<?php echo($qsheet); ?>&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;data1=<?php echo($data_sheet_2); ?>"
           target="rightFrame"><i class="fa fa-angle-double-right"></i> Insérer</a>
        </li>

        <li>
          LISTER les EXERCICES communs à la
          <strong class="feuille">FEUILLE <?php echo ($qsheet); ?></strong>
          et à <strong class="examen">l'EXAMEN <?php echo ($qsheet); ?></strong>
          de la classe WIMS n°<b><?php echo ($qclass); ?></b>
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=listlinks&amp;qsheet=<?php echo($qsheet); ?>&amp;qexam=<?php echo($qsheet); ?>&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>

        <li class="feuille">
          MODIFICATION des caractéristiques (et activation) de la
          <strong>FEUILLE</strong>
          <b><?php echo($qsheet); ?></b>
          de la classe WIMS n°<b><?php echo($qclass); ?></b>
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=modsheet&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;data1=<?php echo($data_sheet); ?>&amp;qsheet=<?php echo($qsheet); ?>"
           target="rightFrame"><i class="fa fa-eye"></i> Activer</a>
        </li>

        <li>
          ATTACHER à <strong class="examen">l'EXAMEN <?php echo ($qsheet); ?></strong>
          <strong class="exercice">l'EXERCICE <?php echo ($qexo); ?></strong>
          de la <strong class="feuille">feuille <?php echo ($qsheet); ?></strong>
          <a class="button small radius"
             href="<?php echo($url); ?>&amp;job=linkexo&amp;qexo=<?php echo($qexo); ?>&amp;qexam=<?php echo($qsheet); ?>&amp;qsheet=<?php echo($qsheet); ?>&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;data1=<?php echo($data_exo); ?>"
             target="rightFrame"><i class="fa fa-link"></i> Attacher</a>
        </li>

        <li class="feuille">
          DESACTIVATION de la
          <strong>FEUILLE</strong>
          <b><?php echo($qsheet); ?></b>
          de la classe WIMS n°<b><?php echo($qclass); ?></b>
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=modsheet&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;data1=status=0&amp;qsheet=<?php echo($qsheet); ?>"
           target="rightFrame"><i class="fa fa-eye-slash"></i> Masquer</a>
        </li>

        <li class="examen">
          MODIFICATION des caractéristiques (et activation) de
          <strong>l'EXAMEN</strong> <b><?php echo($qsheet); ?></b>
          de la classe WIMS n°<b><?php echo($qclass); ?></b>
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=modexam&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;data1=<?php echo($data_sheet); ?>&amp;qexam=<?php echo($qsheet); ?>"
             target="rightFrame"><i class="fa fa-eye"></i> Activer</a>
        </li>

        <li class="examen">
          On CACHE l'EXAMEN <b><?php echo($qsheet); ?></b>
          <small>(pour le moment un EXAMEN ne peut etre desactivé)</small>
          de la classe WIMS n°<b><?php echo($qclass); ?></b>
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=modexam&amp;rclass=<?php echo($rclass); ?>&amp;qclass=<?php echo($qclass); ?>&amp;data1=status=3&amp;qexam=<?php echo($qsheet); ?>"
           target="rightFrame"><i class="fa fa-eye-slash"></i> Masquer</a>
        </li>

        <li>
          On demande les INFOS concernant l'exercice
          <b><?php echo ($qexo); ?></b>
          de la FEUILLE <b><?php echo ($qsheet); ?></b>
          de la classe <b><?php echo ($qclass); ?></b> :
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getexo&amp;qexo=<?php echo($qexo); ?>&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qsheet=<?php echo($qsheet); ?>"
           target="rightFrame"><i class="fa fa-info-circle"></i> infos</a>
        </li>
      </ol>
    </fieldset>

    <fieldset>
      <legend>Autres actions sur une classe</legend>
      <ol class="liste_tests" start="34">

        <li class="feuille">
          LISTER les
          <strong>FEUILLES</strong>
          de la classe <b><?php echo ($qclass); ?></b> :
          <a class="button small radius"
             href="<?php echo($url); ?>&amp;job=listsheets&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
             target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>
        <li class="examen">
          LISTER les
          <strong>EXAMENS</strong>
          de la classe <b><?php echo ($qclass); ?></b> :
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=listexams&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>
        <li class="feuille">
          SUPPRIMER la
          <strong>FEUILLE</strong>
          <b><?php echo ($qsheet); ?></b>
          de la classe <b><?php echo ($qclass); ?></b> :
          <a class="button small radius alert" href="<?php echo($url); ?>&amp;job=delsheet&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qsheet=<?php echo($qsheet); ?>"
           target="rightFrame"><i class="fa fa-trash"></i> Supprimer</a>
        </li>

        <li class="examen">
          SUPPRIMER
          <strong>l'EXAMEN</strong>
          <b><?php echo ($qsheet); ?></b>
          de la classe <b><?php echo ($qclass); ?></b> :
          <a class="button small radius alert" href="<?php echo($url); ?>&amp;job=delexam&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qexam=<?php echo($qsheet); ?>"
           target="rightFrame"><i class="fa fa-trash"></i> Supprimer</a>
        </li>

        <li>
          On demande les NOTES de la classe <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getscores&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>

        <li><strong>NEW (Alpha test)</strong>
          On demande les NOTES de la classe <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getclassscores&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>

        <li>
          On demande les NOTES
          de la FEUILLE <b><?php echo($qsheet); ?></b>
          de la CLASSE  <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getsheetscores&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qsheet=<?php echo($qsheet); ?>"
           target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>
        <li>
          On demande les NOTES
          de l'EXAMEN  <b><?php echo($qsheet); ?></b>
          de la CLASSE <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getexamscores&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qexam=<?php echo($qsheet); ?>"
           target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>

        <li>
          On demande les STATS
          de la FEUILLE <b><?php echo($qsheet); ?></b>
          de la CLASSE  <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getsheetstats&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qsheet=<?php echo($qsheet); ?>"
           target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>

        <li class="classe">
          PURGER la <strong>CLASSE</strong>
          WIMS n°<b><?php echo ($qclass); ?></b>
          <a class="button small radius warning" href="<?php echo($url); ?>&amp;job=cleanclass&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-filter"></i> Purger</a>
        </li>

        <li class="classe">
          REPOUSSER la date d'expiration de la <strong>CLASSE</strong>
          WIMS n°<b><?php echo ($qclass); ?> et de toutes les activités (feuilles et examens)</b>
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=changedates&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-calendar"></i> PROLONGER</a>
        </li>

        <li class="classe">
          SUPPRIMER la <strong>CLASSE</strong>
          WIMS n°<b><?php echo ($qclass); ?></b>
          <a class="button small radius alert" href="<?php echo($url); ?>&amp;job=delclass&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-trash"></i> Supprimer</a>
        </li>
        <?php  $option=" ";  ?>
      </ol>
    </fieldset>

    <fieldset>
      <ol class="liste_tests" start="43">
      <legend>Fonctions du serveur WIMS :</legend>
        <li>
          LISTER les <strong>Catégories</strong>
          du serveur :
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=listmodules&amp;option=<?php echo($option); ?>"
           target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>
        <?php  $option="/E1"; ?>
        <li>
          LISTER les <strong>Domaines</strong> de la catégorie "E1"
          du serveur :
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=listmodules&amp;option=<?php echo($option); ?>"
           target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>
        <?php  $option="/E1/geometry"; ?>
        <li>
          LISTER les <strong>Modules</strong> du domaine "E1/geometry"
          du serveur :
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=listmodules&amp;option=<?php echo($option); ?>"
           target="rightFrame"><i class="fa fa-list"></i> Lister</a>
        </li>

        <li>
          Obtenir les INFOS du <strong>Module</strong>
          <b><?php echo($exo_module); ?></b> :
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getmodule&amp;option=<?php echo($exo_module); ?>"
           target="rightFrame"><i class="fa fa-info-circle"></i> infos</a>
        </li>

        <li>
          LANCER un exercice du <strong>Module <?php echo($exo_module); ?> (mode "lightpopup")</strong> :
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=lightpopup&amp;emod=<?php echo($exo_module); ?>&amp;parm=cmd=new"
           target="rightFrame"><i class="fa fa-terminal"></i> Tester</a>
        </li>
      </ol>
    </fieldset>

    <hr/>

    <fieldset>
      <legend>Fonctions sur les FICHIERS :</legend>
      <ol class="liste_tests" start="45">
        <li>
          On demande le FICHIER au format CSV concernant la classe <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius download" href="<?php echo($url); ?>&amp;job=getcsv&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-download"></i> Télécharger</a>
        </li>
        <li>
          On demande le FICHIER au format CSV (adapté pour XLS francais) concernant la classe <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius download" href="<?php echo($url); ?>&amp;job=getcsv&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;format=xls"
           target="rightFrame"><i class="fa fa-download"></i> Télécharger</a>
        </li>
        <li>
          <strong class="debug">BETA</strong> On demande le FICHIER au format ODS (Open/Libre Office) concernant la classe <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getcsv&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;format=ods"
           target="rightFrame"><i class="fa fa-download"></i> Télécharger</a>
        </li>

        <li>
          On demande le FICHIER
          <b><?php echo($fname); ?></b>
          de la classe <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius download" href="<?php echo($url); ?>&amp;job=getclassfile&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;option=<?php echo($fname); ?>"
           target="rightFrame"><i class="fa fa-download"></i> Télécharger</a>
        </li>

        <li>
          On demande le FICHIER EXERCICE
          <b><?php echo($exoID); ?></b>
          de la classe <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getexofile&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qexo=<?php echo($exoID); ?>"
           target="rightFrame"><i class="fa fa-download"></i> Télécharger</a>
        </li>
        <li>
          On demande le FICHIER COMPRESSE(TGZ) de la classe <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius download" href="<?php echo($url); ?>&amp;job=getclasstgz&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-download"></i> Télécharger</a>
        </li>

        <li>
          On
          <strong>ENVOIE</strong>
          un FICHIER d'exercice
          <strong><?php echo($exoID); ?></strong>
          &agrave; la CLASSE
          <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=addexo&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qexo=<?php echo($exoID); ?>&amp;data1=<?php echo($dataOEF); ?>"
             target="rightFrame"><i class="fa fa-paper-plane"></i> Envoyer</a>
        </li>
        <li>
          On
          <strong>ENVOIE</strong>
          (en FORCANT la réécriture) un FICHIER d'exercice
          <strong>
            <?php echo($exoID); ?></strong>
          &agrave; la CLASSE
          <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=addexo&amp;option=force_rewrite&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qexo=<?php echo($exoID); ?>&amp;data1=<?php echo($data1); ?>"
             target="rightFrame"><i class="fa fa-paper-plane"></i> Envoyer</a>
        </li>
        <li>
          On
          <strong>SUPPRIME</strong>
          l'EXERCICE
          <strong>
            <?php echo($exoID); ?></strong>
          de la CLASSE
          <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius alert" href="<?php echo($url); ?>&amp;job=delexo&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qexo=<?php echo($exoID); ?>"
             target="rightFrame"><i class="fa fa-trash"></i> Supprimer</a>
          </li>

        <?php $data2="\\title{ESSAI FOIREUX}%0D%0A";?>
        <li>
          On
          <strong>ENVOIE</strong>
          un FICHIER d'exercice volontairement ERRON&Eacute;
          <strong>
            <?php echo($exoID); ?></strong>
          &agrave; la CLASSE
          <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius"
             href="<?php echo($url); ?>&amp;job=addexo&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;qexo=<?php echo($exoID); ?>&amp;data1=<?php echo($data2); ?>"
             target="rightFrame"><i class="fa fa-paper-plane"></i> Envoyer</a>
        </li>

        <li>
          On demande les <strong>logs de connexion</strong> de l'eleve
          <strong><?php echo($quser); ?></strong>
          de la CLASSE <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getlog&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;quser=<?php echo($quser); ?>"
           target="rightFrame"><i class="fa fa-info-circle"></i> infos</a>
        </li>

        <li>
          On demande les <strong>logs d'exams</strong> de l'eleve
          <strong><?php echo($quser); ?></strong>
          de la CLASSE <b><?php echo($qclass); ?></b>
          sur wims liée à notre classe locale
          <a class="button small radius" href="<?php echo($url); ?>&amp;job=getexamlog&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>&amp;quser=<?php echo($quser); ?>"
             target="rightFrame"><i class="fa fa-info-circle"></i> infos</a>
        </li>
      </ol>
    </fieldset>

    <hr/>

    <fieldset>
      <legend>Autres Fonctions diverses :</legend>
      <ul>
      <li>
        Bonus 1 - Afficher l'heure du serveur WIMS :
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=gettime" target="rightFrame"><i class="fa fa-clock-o"></i> Horloge</a>
      </li>

      <?php $qclass=1114; ?>

      <li>
        Bonus 2 - Test d'acces à la classe
        <b><?php echo($qclass); ?></b>
        sur wims , non liée à notre classe locale
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=getclass&amp;qclass=<?php echo($qclass); ?>&amp;rclass=<?php echo($rclass); ?>"
           target="rightFrame"><i class="fa fa-terminal"></i> Tester</a>
      </li>
      <?php
        $search_category="A";
        $search_keywords="geography";
        ?>
      <li>
        Bonus 3 - ajouter ceci à la fin d'une URL pour effectuer une recherche sur "
        <?php echo($search_keywords); ?>
        " dans sa classe : &amp;search_keywords=<strong><?php echo($search_keywords); ?></strong>&amp;search_category=<?php echo($search_category); ?>
      </li><li>
        <strong class="debug">BETA</strong>
        Bonus 4 - Rechercher
        <b><?php echo($search_keywords); ?></b>
        dans la categorie
        <b><?php echo($search_category); ?></b>
        <a class="button small radius" href="<?php echo($url); ?>&amp;job=search&amp;search_category=<?php echo($search_category); ?>&amp;search_keywords=<?php echo($search_keywords); ?>"
           target="rightFrame"><i class="fa fa-terminal"></i> Tester</a>
      </li>
    </ul>
    </fieldset>

