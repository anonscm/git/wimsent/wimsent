<?php
  /************* PARAMETRES A PERSONNALISER **********/
  $serveur="URL_DU_SERVEUR_WIMS"; /* exemple : devwims.unice.fr */
  $protocol="http"; /* http ou https */
  $local_ident="IDENTIFIANT_DU_SERVEUR_DISTANT"; /* exemple : jalon */
  $local_pass="PASSWORD_DU_SERVEUR_DISTANT";
  $qclass  = "1234567"; // Id d'une classe a tester sur WIMS
  $sous_classe = "_2"; // Sous-classe à tester, dans le cas où $qclass est un groupement de classes
  /***************************************************/

/* NB : Ces infos doivent être en rapport avec les infos de connections sur votre serveur WIMS.
  cf $WIMS_HOME/log/classes/.connections/
*/
