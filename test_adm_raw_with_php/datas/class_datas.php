<?php
/************************************************************
   Definitions des parametres de creation d'une classe WIMS
*************************************************************/

/**********
 DATA 1
***********/
$typenames=array("class","","group","","portal");
if (!isset($type)) {
    $type=0;
}

//Liste de paramètres primaires obligatoire pour la création d'une classe :
$data_1="description=Classe_test\n";      //Nom de la classe
$data_1=$data_1."institution=Mon ETABLISSEMENT\n";   //Nom de l'institution
$data_1=$data_1."supervisor=PROF de TEST\n"; //Nom complet du superviseur affiché à l'entrée de la classe
$data_1=$data_1."email=moi@domaine.fr\n"; //email du superviseur
$data_1=$data_1."password=azerty\n";      //Mot de passe pour l'inscription des participants

/***************************************
Les parametres primaires suivants sont OPTIONNELS
****************************************/
$data_1=$data_1."lang=fr\n";           //Langue de la classe (par défaut : en)
/*
$data_1=$data_1."expiration=20080101\n";  //Date d'expiration (par défaut : un an après)
$data_1=$data_1."limit=100\n";         //Limite du nombre de participants (par défaut : 30)
$data_1=$data_1."topscores=10\n";      //scores max ?
$data_1=$data_1."level=U1\n";       //Niveau de la classe (par défaut : H4)
*/
$data_1=$data_1."secure=all\n";     //IP sécurisées (postes sûrs : si non défini, email de confirmation)
/*
$data_1=$data_1."superclass=\n";       //Superclass ?
*/
$data_1=$data_1."type=".$type."\n"; //id du type de classe (0 : classe independante, 2 : groupement, 4:portail)
$data_1=$data_1."typename=".$typenames[$type]."\n";   //Type de classe (class,group,portal)
$data_1=$data_1."theme=standard\n"; //Theme utilisé pour l'affichage de la classe
$data_1=$data_1."css=-theme-\n";       //Fichier CSS utilisé (sur wims). par défaut : "-theme-", utilise le css du theme
//$data_1=$data_1."theme_icon=wimsedu\n";  //Set d'icones utilisé
/*
$data_1=$data_1."connections=+jalonsante/ClasseJalon+\n";

$data_1=$data_1."logo=\n";                     //Logo
$data_1=$data_1."logoside=\n";                 //alignement du logo
$data_1=$data_1."bgcolor=000022\n";            //Couleur de fond de la page
$data_1=$data_1."bgimg=\n";                    //image de fond de la page
$data_1=$data_1."scorecolor=\n";               //Couleurs des paliers (par défaut : white,red,red,red,orange,orange,orange,yellow,yellow,green,green)
$data_1=$data_1."refcolor=#220000\n";          //Couleur des Menus
$data_1=$data_1."ref_menucolor=\n";            //Couleur des Menus
$data_1=$data_1."ref_button_color=\n";         //Couleur des boutons
$data_1=$data_1."ref_button_bgcolor=\n";       //Couleur de fond des boutons
$data_1=$data_1."ref_button_help_color=\n";    //Couleur des boutons d'aide
$data_1=$data_1."ref_button_help_bgcolor=\n";  //Couleur de fond des boutons d'aide
$data_1=$data_1."creator=\n";                  //Id de l'ordinateur utilisé pour ajouter la classe
*/
$data_1 = urlencode($data_1);

/**
 * Données volontairement erronées pour les tests
 **/

$data_err1="description=Alors ca c'est un nom super long pour une classe WIMS\n"; //Nom de la classe (trop long - sera tronqué à "super lo")
$data_err1=$data_err1."institution=CLASSE,!\"<0001\n"; //Mix avec Caractères interdits (seront remplacés par un espace)
// $data_err1=$data_err1."supervisor=AB\n";        //Nom du superviseur trop court
$data_err1=$data_err1."supervisor=PROF de TEST\n"; //Nom du superviseur OK
$data_err1=$data_err1."email=test.fr\n";   //email incorrect
$data_err1=$data_err1."password=aze\n";           //Mot de passe trop court

$data_err1 = urlencode($data_err1);

/**********
 DATA 2
***********/

//Liste de paramètres secondaires OBLIGATOIRES pour la création d'une classe :
$data_2 = "lastname=MonNom\n";           //Nom du superviseur
$data_2 = $data_2."firstname=MonPrenom\n"; //Prénom du superviseur
$data_2 = $data_2."password=azertop";      //Mot de passe non crypté de l'admin

/**************************************************
Les parametres secondaires suivants sont OPTIONNELS
***************************************************
$data_2=$data_2."email=kiki@unice.fr";        //email address (optional)
$data_2=$data_2."comments="mes commentaires"; //any comments (optional)
$data_2=$data_2."regnum=1234";                //registration number (optional)
*/
$data_2 = urlencode($data_2);


/**
 * Données volontairement erronées pour les tests
 **/
$data_err2 = "lastname=DE,!\"<TEST\n";      //Caractères interdits
$data_err2 = $data_err2."firstname=P\n"; //Prénom du superviseur trop court
$data_err2 = $data_err2."password=aze\n";  //Mot de passe trop court

$data_err2 = urlencode($data_err2);

/**********
 Parametres généraux
***********/

/* Quelques variables communes pour les tests */
$rclass  = "nouvelleClasse";
$rand_code="0a1234";
$fname="sheets/.sheets";
$lang="fr";

/**********
 Parametres utilisateur
***********/

$quser="etuTest3"; //login du user
//Paramètres obligatoires pour la création d'un user :
$data_user = "lastname=Etudiant\n";           //Nom du user
$data_user = $data_user."firstname=DeTest\n"; //Prénom du user
$data_user = $data_user."password=azertop\n"; //Mot de passe (non crypté)
$data_user = $data_user."custom_var1=cccc";   //Exemple de variable technique (facultatif)
$data_user = urlencode($data_user);

/**********
 Données de la classe (CSV)
***********/

$datacsv = 'login,lastname,firstname\n"#","Family name","First (given) name",user1,John,Doe\nuser2,Janette,Doe\n';
