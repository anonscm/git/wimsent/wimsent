<?php

  //adresse d'un module à tester
   $module_test = "E6/geometry/oefrect.fr";

  /************************************************************
     Definitions des parametres de creation d'un EXAM WIMS
  *************************************************************/

  //Paramètres optionels pour la création d'un examen :
   $data_exam = "expiration=20120219\n";      //Date d'expiration
   $data_exam = $data_exam."title=Encore un Exam'\n"; //Titre de l'examen
   $data_exam = $data_exam."duration=45\n"; //Durée de l'examen
   $data_exam = $data_exam."attempts=2\n";  //Nombre d'essais autorisés
   $data_exam = $data_exam."cut_hours=\n";  //Heures de coupure
  //Format des heures de coupure: aaaammjj.hh:mm (séparez plusieurs heures de coupure par des espaces).
   $data_exam = $data_exam."description=Bienvenue dans mon examen\n"; //Titre de l'examen
   $data_exam = $data_exam."opening=\n";  //Restrictions d'ouverture (plages IP/Horaires)
   /*opening peut dfinir une restriction dans le temps pour l'enregistrement des notes en ajoutant les mots >
   //   aaaammjj.hh:mm (début) et/ou
   //   <aaaammjj.hh:mm (fin).
   // Les dates et heures doivent être en temps local du SERVEUR et ces mots doivent être séparés par des espaces.*/
   $data_exam = urlencode($data_exam);

  /************************************************************
      Definitions des parametres de creation d'une FEUILLE WIMS
  *************************************************************/

   $qsheet=2; //numero de la feuille à modifier

  //Paramètres optionels pour la création d'une feuille :
   $data_sheet_1 = "expiration=20150219\n";     //Date d'expiration
   $data_sheet_1 = $data_sheet_1."title=Encore une Feuille\n";  //Titre de la feuille
   $data_sheet_1 = $data_sheet_1."weight=8\n";    // poids de la feuille dans le calcul de la note de la classe.
   $data_sheet_1 = $data_sheet_1."formula=3\n";   // one of the 7 availables formulas (see adm/class/sheetformula for details)
                                                  // [max(I,Q), I, I*Q^0.3, I*Q^0.5, I*Q, I^2*Q, (I*Q)^2]
   $data_sheet_1 = $data_sheet_1."indicator=0\n";   //severity Indicator (I0,I1,I2) (I in the previous formula)
   $data_sheet_1 = $data_sheet_1."description=Ma nouvelle feuille d'exercices";   //Description  de la feuille
   $data_sheet_1 = urlencode($data_sheet_1);

  //Paramètres optionels pour la modification d'une feuille :
   $data_sheet = "status=1\n";      //Statut de la feuille : 0-en prepa/1-active/2-périmée/3-périmée+cachée
   $data_sheet = $data_sheet."title=ma feuille de renom\n"; //titre de la feuille
   $data_sheet = $data_sheet."expiration=30000202\n";     //date d'expiration
   $data_sheet = $data_sheet."description=c'est une super feuille"; //Description  de la feuille
   $data_sheet = urlencode($data_sheet);


/** Exercice de la classe **/
   //parametres obligatoires pour l'ajout d'un exercice à une feuille :
   $data_exo_classe = "module=classes/fr\n";  //adresse du module
   //Paramètres pour l'ajout d'un exo dans une feuille :
   $data_exo_classe = $data_exo_classe."exo=Choixmultiplem&qnum=1&qcmlevel=1&intro_qcmpresent=4&intro_presentsol=1&intro_check=1&intro_check=2&intro_check=4&intro_expert=yes\n"; //liste des parametres a ajouter au module
   $data_exo_classe = $data_exo_classe."points=45\n";     //Nombre de points demandés
   $data_exo_classe = $data_exo_classe."weight=2\n";        //Poid de l'exercice
   $data_exo_classe = $data_exo_classe."title=Choix multiple a moi\n";  //titre de l'exercice
   $data_exo_classe = $data_exo_classe."description=collection d'exercices sur les rectangles";   //Description  de l'exercice
   $data_exo_classe = urlencode($data_exo_classe);

/** Exercice publié **/
   $exo_params = "exo=addfourier2&exo=addfourier1&exo=addfourier4&exo=addfourier3&qnum=1&qcmlevel=3";
   $exo_module = "U2/analysis/oeffourier.fr";

   $data_sheet_2 = "module=".$exo_module."\n";  //adresse du module
    //Paramètres optionels pour la création d'une feuille :
   $data_sheet_2 = $data_sheet_2."params=".$exo_params."\n";  //liste des parametres a ajouter au module
   $data_sheet_2 = $data_sheet_2."points=45\n";     //Nombre de points demandés
   $data_sheet_2 = $data_sheet_2."weight=2\n";      //Poid de l'exercice
   $data_sheet_2 = $data_sheet_2."title=addition de fourier au hasard\n";  //titre de l'exercice
   $data_sheet_2 = $data_sheet_2."description=un exo au hasard parmi les additions de fourier";   //Description  de l'exercice
   $data_sheet_2= urlencode($data_sheet_2);


   /************************************************************
      Definitions des parametres de creation d'un EXO WIMS
   *************************************************************/
   $qexo=1;
  //Paramètres optionels pour attacher l'exercice d'une feuille a un examen :
   $data_exo = "weight=1\n";      //Statut de la feuille : 0-en prepa/1-active/2-périmée/3-périmée+cachée
   $data_exo = $data_exo."title=mon exo dans mon exam\n"; //titre de la feuille
   $data_exo = $data_exo."description=description de mon exo dans mon exam\n";      //date d'expiration
   //$data_exo = $data_exo."dependences=1:25\n";  //dependences de la feuille
   $data_exo = $data_exo."dependences=\n";  //dependences de la feuille
   $data_exo = urlencode($data_exo);

// par defaut, weight est pioche dans la feuille
// par defaut, title est pioche dans la feuille
// par defaut, description est pioche dans la feuille
// par defaut, dependences est vide
// par defaut, autogen est vide  (peut etre soit vide, soit "autogen" )

/* Prametres de creation dun exercice OEF dans la classe */
  $exoID = "essai123";
  $dataOEF = "\\title{ESSAI DE TRANSFERT}%0D%0A
\\text{meschoix=choix1,choix2,choix3,choix4}%0D%0A
\\integer{tot=items(\meschoix)}%0D%0A
\\text{list=item(1..\\tot,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z)}%0D%0A
\\text{anstype=checkbox}%0D%0A
\statement{Cochez la(les) bonne(s) r&eacute;ponse(s)%0D%0A<div align='center'>
        %0D%0A
\\for{i=1 to \\tot}{%0D%0A
 \\embed{reply 1,\i}. \\meschoix[\i,]
        <br/>
        %0D%0A
}%0D%0A
      </div>
      %0D%0A
}%0D%0A
\\answer{La r&eacute;ponse}{1,2;\\list}{type=checkbox}{option=split}%0D%0A";
